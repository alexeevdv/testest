<?php

return [
    'components' => [
        'request' => [
            'baseUrl' => '/admin',
            'cookieValidationKey' => '',
        ],
    ],
    'homeUrl' => '/admin',
];
