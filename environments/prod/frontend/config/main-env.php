<?php

return [
    'components' => [
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => '',
        ],
    ],
    'homeUrl' => '/',
];
