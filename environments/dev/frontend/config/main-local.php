<?php

return [
    'bootstrap' => [
        'debug',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '',
            'cookieValidationKey' => '',
        ],
    ],
    'homeUrl' => '/',
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
    ],
];
