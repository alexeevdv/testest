<?php

return [
    'bootstrap' => [
        'debug',
    ],
    'components' => [
        'request' => [
            'baseUrl' => '/admin',
            'cookieValidationKey' => '',
        ],
    ],
    'homeUrl' => '/admin',
    'modules' => [
        'debug' => [
            'class' => 'yii\debug\Module',
        ],
    ],

];
