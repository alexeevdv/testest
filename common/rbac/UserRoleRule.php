<?php

namespace common\rbac;

use common\models\User;
use yii\rbac\Rule;

/**
 * Class UserRoleRule
 * @package common\rbac
 */
class UserRoleRule extends Rule
{
    public $name = 'rule_userRole';

    /**
     * @inheritdoc
     */
    public function execute($user, $item, $params)
    {
        $model = User::findOne($user);
        if (!$model) {
            return false;
        }

        if ($item->name === 'admin') {
            return $model->role === 'admin';
        } elseif ($item->name === 'user') {
            return in_array($model->role, ['admin', 'user']);
        }

        return false;
    }
}
