<?php
return [
    'user' => [
        'type' => 1,
        'ruleName' => 'rule_userRole',
    ],
    'admin' => [
        'type' => 1,
        'ruleName' => 'rule_userRole',
        'children' => [
            'user',
        ],
    ],
];
