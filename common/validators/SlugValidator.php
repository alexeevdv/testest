<?php

namespace common\validators;

use yii\validators\RegularExpressionValidator;

/**
 * Class SlugValidator
 * @package common\validators
 */
class SlugValidator extends RegularExpressionValidator
{
    /**
     * @inheritdoc
     */
    public $pattern = '/^[a-z-_0-9]+$/i';
}
