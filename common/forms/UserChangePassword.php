<?php

namespace common\forms;

use yii\base\Model;

/**
 * Class UserChangePassword
 * @package common\forms
 */
class UserChangePassword extends Model
{
    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $password_confirmation;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['password', 'safe'],
            ['password_confirmation', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'password_confirmation' => 'Подтверждение пароля',
        ];
    }
}
