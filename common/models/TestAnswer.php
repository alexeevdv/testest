<?php

namespace common\models;

use common\models\queries\TestAnswerQuery;
use common\models\queries\TestQuestionQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class TestAnswer
 * @package common\models
 *
 * @property int $id
 * @property int $question_id
 * @property string $text
 * @property boolean $is_right
 *
 * @property TestQuestion $question
 */
class TestAnswer extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%test_answer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['text', 'trim'],
            [['question_id', 'text', 'is_right'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст ответа',
            'is_right' => 'Правильный или неправильный'
        ];
    }

    /**
     * @return TestQuestionQuery
     */
    public function getQuestion()
    {
        /** @var TestQuestionQuery $query */
        $query = $this->hasOne(TestQuestion::class, ['id' => 'question_id']);
        return $query;
    }

    /**
     * @return TestAnswerQuery
     */
    public static function find()
    {
        return Yii::createObject(TestAnswerQuery::class, [get_called_class()]);
    }
}
