<?php

namespace common\models;

use common\models\queries\ForumSectionQuery;
use common\models\queries\ForumThemeQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ForumSection
 * @package common\models
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ForumTheme[] $themes
 * @property ForumSection $parent
 * @property ForumSection[] $children
 */
class ForumSection extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%forum_section}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'trim'],
            ['title', 'required'],
            ['parent_id', 'default']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'parent_id' => 'Родительский раздел',
            'title' => 'Название',
            'description' => 'Краткое описание',
            'created_at' => 'Дата создания'
        ];
    }

    /**
     * @return ForumThemeQuery
     */
    public function getThemes()
    {
        /** @var ForumThemeQuery $query */
        $query = $this->hasMany(ForumTheme::class, ['section_id' => 'id']);
        return $query;
    }

    /**
     * @return ForumSectionQuery
     */
    public function getParent()
    {
        /** @var ForumSectionQuery $query */
        $query = $this->hasOne(ForumSection::class, ['id' => 'parent_id']);
        return $query;
    }

    /**
     * @return ForumSectionQuery
     */
    public function getChildren()
    {
        /** @var ForumSectionQuery $query */
        $query = $this->hasMany(ForumSection::class, ['parent_id' => 'id']);
        return $query;
    }

    /**
     * @return ForumSectionQuery
     */
    public static function find()
    {
        return Yii::createObject(ForumSectionQuery::class, [get_called_class()]);
    }
}
