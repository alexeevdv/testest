<?php

namespace common\models;

use common\models\queries\QuestionAndAnswerQuery;
use common\models\queries\UserQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class QuestionAndAnswer
 * @package common\models
 *
 * @property int $id
 * @property int $question_user_id
 * @property string $question
 * @property int $answer_user_id
 * @property string $answer
 * @property string $created_at
 * @property string $updated_at
 * @property bool $is_enabled
 *
 * @property User[] $askingUsers
 * @property User[] $respondingUsers
 */
class QuestionAndAnswer extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%question_and_answer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['question', 'trim'],
            [['question', 'question_user_id'], 'required'],
            [['answer', 'answer_user_id'], 'default'],
            ['is_enabled', 'boolean'],
            ['is_enabled', 'default', 'value' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'created_at' => 'Когда задан',
            'is_enabled' => 'Отображать',
        ];
    }

    /**
     * @inheritdoc
     * @return QuestionAndAnswerQuery
     */
    public static function find()
    {
        return Yii::createObject(QuestionAndAnswerQuery::class, [get_called_class()]);
    }

    /**
     * @return UserQuery
     */
    public function getAskingUsers()
    {
        /** @var UserQuery $query */
        $query = $this->hasMany(User::class, ['id' => 'question_user_id']);
        return $query;
    }

    /**
     * @return UserQuery
     */
    public function getRespondingUsers()
    {
        /** @var UserQuery $query */
        $query = $this->hasMany(User::class, ['id' => 'answer_user_id']);
        return $query;
    }
}
