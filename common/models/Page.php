<?php

namespace common\models;

use common\models\queries\PageQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression as DbExpression;

/**
 * Class Page
 * @package common\models
 */
class Page extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'seo_title', 'seo_keywords', 'seo_description'], 'trim'],
            [['name', 'slug'], 'required'],
            [['text', 'seo_title', 'seo_keywords', 'seo_description'], 'default'],
            ['published', 'default', 'value' => false],
            ['published', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new DbExpression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'slug' => 'URL алиас',
            'text' => 'Текст',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
            'published' => 'Опубликована',
        ];
    }

    /**
     * @inheritdoc
     * @return PageQuery
     */
    public static function find()
    {
        return Yii::createObject(PageQuery::class, [get_called_class()]);
    }
}
