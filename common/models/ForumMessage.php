<?php

namespace common\models;

use common\models\queries\ForumMessageQuery;
use common\models\queries\ForumThemeQuery;
use common\models\queries\UserQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ForumMessage
 * @package common\models
 *
 * @property integer $id
 * @property integer $theme_id
 * @property string $text
 * @property boolean $is_moderated
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ForumTheme $theme
 * @property User $author
 */
class ForumMessage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%forum_message}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['text', 'trim'],
            [['theme_id', 'text', 'user_id'], 'required'],
            ['is_moderated', 'boolean'],
            ['is_moderated', 'default', 'value' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
            'is_moderated' => 'Прошел модерацию',
            'created_at' => 'Дата создания'
        ];
    }

    /**
     * @return ForumThemeQuery
     */
    public function getTheme()
    {
        /** @var ForumThemeQuery $query */
        $query = $this->hasOne(ForumTheme::class, ['id' => 'theme_id']);
        return $query;
    }

    /**
     * @return UserQuery
     */
    public function getAuthor()
    {
        /** @var UserQuery $query */
        $query = $this->hasOne(User::class, ['id' => 'user_id']);
        return $query;
    }

    /**
     * @return ForumMessageQuery
     */
    public static function find()
    {
        return Yii::createObject(ForumMessageQuery::class, [get_called_class()]);
    }
}
