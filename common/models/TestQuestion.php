<?php

namespace common\models;

use common\models\queries\TestAnswerQuery;
use common\models\queries\TestQuery;
use common\models\queries\TestQuestionQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class TestQuestion
 * @package common\models
 *
 * @property integer $id
 * @property integer $test_id
 * @property string $text
 * @property boolean $is_checkbox
 * @property string $comment
 *
 * @property TestAnswer[] $answers
 * @property Test $test
 */
class TestQuestion extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%test_question}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'comment'], 'trim'],
            ['is_checkbox', 'boolean'],
            [['test_id', 'text'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст вопроса',
            'is_checkbox' => 'Тип вопроса',
            'comment' => 'Комментарий при неправильном ответе'
        ];
    }

    /**
     * @return TestAnswerQuery
     */
    public function getAnswers()
    {
        /** @var TestAnswerQuery $query */
        $query = $this->hasMany(TestAnswer::class, ['question_id' => 'id']);
        return $query;
    }

    /**
     * @return TestQuery
     */
    public function getTest()
    {
        /** @var TestQuery $query */
        $query = $this->hasOne(Test::class, ['id' => 'test_id']);
        return $query;
    }

    /**
     * @return TestQuestionQuery
     */
    public static function find()
    {
        return Yii::createObject(TestQuestionQuery::class, [get_called_class()]);
    }
}
