<?php

namespace common\models;

use common\models\queries\OrganizationQuery;
use common\models\queries\OrganizationTypeQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class OrganizationType
 * @package common\models
 *
 * @property $id
 * @property $name
 *
 * @property Organization[] $organizations
 */
class OrganizationType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%organization_type}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название типа'
        ];
    }

    /**
     * @return OrganizationQuery
     */
    public function getOrganizations()
    {
        /** @var OrganizationQuery $query */
        $query = $this
            ->hasMany(Organization::class, ['id' => 'organization_id'])
            ->viaTable('organization_to_type', ['type_id' => 'id']);
        return $query;
    }

    /**
     * @inheritdoc
     * @return OrganizationTypeQuery
     */
    public static function find()
    {
        return Yii::createObject(OrganizationTypeQuery::class, [get_called_class()]);
    }
}
