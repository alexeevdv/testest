<?php

namespace common\models;

use common\models\queries\ForumMessageQuery;
use common\models\queries\ForumSectionQuery;
use common\models\queries\ForumThemeQuery;
use common\models\queries\UserQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ForumTheme
 * @package common\models
 *
 * @property integer $id
 * @property integer $section_id
 * @property integer $user_id
 * @property string $title
 * @property boolean $is_closed
 * @property boolean $is_pinned
 * @property string $created_at
 * @property string $updated_at
 *
 * @property string $authorName
 * @property string $sectionTitle
 *
 * @property ForumSection $section
 * @property User $author
 * @property ForumMessage $messages
 */
class ForumTheme extends ActiveRecord
{
    /** @var  string */
    public $authorName;

    /** @var  string */
    public $sectionTitle;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%forum_theme}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'trim'],
            [['user_id', 'section_id'], 'default'],
            [['is_closed', 'is_pinned'], 'boolean'],
            [['is_closed', 'is_pinned'], 'default', 'value' => false],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'is_closed' => 'Тема закрыта',
            'is_pinned' => 'Тема закреплена',
            'created_at' => 'Дата создания'
        ];
    }

    /**
     * @return ForumSectionQuery
     */
    public function getSection()
    {
        /** @var ForumSectionQuery $query */
        $query = $this->hasOne(ForumSection::class, ['id' => 'section_id']);
        return $query;
    }

    /**
     * @return UserQuery
     */
    public function getAuthor()
    {
        /** @var UserQuery $query */
        $query = $this->hasOne(User::class, ['id' => 'user_id']);
        return $query;
    }

    /**
     * @return ForumMessageQuery
     */
    public function getMessages()
    {
        /** @var ForumMessageQuery $query */
        $query = $this->hasMany(ForumMessageQuery::class, ['theme_id' => 'id']);
        return $query;
    }

    /**
     * @return ForumThemeQuery
     */
    public static function find()
    {
        return Yii::createObject(ForumThemeQuery::class, [get_called_class()]);
    }
}
