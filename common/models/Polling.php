<?php

namespace common\models;

use common\models\queries\PollingAnswerQuery;
use common\models\queries\PollingQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class Polling
 * @package common\models
 *
 * @property integer $id
 * @property string $name
 * @property boolean $is_active
 *
 */
class Polling extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polling}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['name'], 'required'],
            [['is_active'], 'default', 'value' => false],
            ['is_active', 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'is_active' => 'Активен',
        ];
    }

    /**
     * @inheritdoc
     * @return PollingQuery
     */
    public static function find()
    {
        return Yii::createObject(PollingQuery::class, [get_called_class()]);
    }

    /**
     * @return PollingAnswerQuery
     */
    public function getAnswers()
    {
        return $this->hasMany(PollingAnswer::class, ['polling_id' => 'id']);
    }
}
