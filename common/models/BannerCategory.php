<?php

namespace common\models;

use common\models\queries\BannerCategoryQuery;
use common\models\queries\BannerQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class BannerCategory
 * @package common\models
 */
class BannerCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position'], 'required'],
            [['name', 'position'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Категория',
            'position' => 'Позиция',
        ];
    }

    /**
     * @inheritdoc
     * @return BannerCategoryQuery
     */
    public static function find()
    {
        return Yii::createObject(BannerCategoryQuery::className(), [get_called_class()]);
    }

    /**
     * @return BannerQuery
     */
    public function getBanners()
    {
        /** @var BannerQuery $query */
        $query = $this->hasMany(Banner::class, ['category_id' => 'id']);
        return $query;
    }
}
