<?php

namespace common\models\search;

use common\models\Polling;
use yii\data\ActiveDataProvider;

/**
 * Class PollingSearch
 * @package common\models
 */
class PollingSearch extends Polling
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['is_active', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = Polling::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['is_active' => $this->is_active]);

        return $dataProvider;
    }
}
