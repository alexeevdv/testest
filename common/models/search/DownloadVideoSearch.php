<?php

namespace common\models\search;

use common\models\DownloadVideo;
use yii\data\ActiveDataProvider;

/**
 * Class DownloadVideoSearch
 * @package common\models\search
 */
class DownloadVideoSearch extends DownloadVideo
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = DownloadVideo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
