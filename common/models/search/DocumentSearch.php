<?php

namespace common\models\search;

use common\models\Document;
use yii\data\ActiveDataProvider;

/**
 * Class DocumentSearch
 * @package common\models\search
 */
class DocumentSearch extends Document
{
    /** @var string  */
    public $start_date_date_filter = '=';

    /** @var string  */
    public $end_date_date_filter = '=';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'author', 'start_date', 'end_date'], 'trim'],
            [['start_date_date_filter', 'end_date_date_filter'], 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = Document::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'author', $this->author]);

        if (!empty($this->start_date)) {
            $query->andFilterWhere([$this->start_date_date_filter, 'start_date', $this->start_date]);
        }

        if (!empty($this->end_date)) {
            $query->andFilterWhere([$this->end_date_date_filter, 'end_date', $this->end_date]);
        }

        return $dataProvider;
    }
}
