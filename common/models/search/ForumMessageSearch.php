<?php

namespace common\models\search;

use common\models\ForumMessage;
use yii\data\ActiveDataProvider;

/**
 * Class ForumMessageSearch
 * @package common\models\search
 */
class ForumMessageSearch extends ForumMessage
{
    /** @var  string */
    public $authorName;

    /** @var  string */
    public $themeTitle;

    /** @var string  */
    public $created_at_date_filter = '=';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'is_moderated', 'themeTitle', 'created_at', 'authorName'], 'trim'],
            ['created_at_date_filter', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = ForumMessage::find();
        $query->joinWith('theme theme');
        $query->joinWith('author author');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'text',
                    'is_moderated',
                    'created_at',
                    'themeTitle' => [
                        'asc' => ['theme.title' => SORT_ASC],
                        'desc' => ['theme.title' => SORT_DESC],
                        'default' => ['theme.title' => SORT_DESC],
                    ],
                    'authorName' => [
                        'asc' => ['author.name' => SORT_ASC],
                        'desc' => ['author.name' => SORT_DESC],
                        'default' => ['author.name' => SORT_DESC],
                    ]
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'text', $this->text]);

        $query->andFilterWhere(['is_moderated' => $this->is_moderated]);

        if (!empty($this->created_at)) {
            $query->andFilterWhere([$this->created_at_date_filter, 'created_at', $this->created_at]);
        }

        $query->andFilterWhere(['like', 'theme.title', $this->themeTitle]);

        $query->andFilterWhere(['like', 'author.name', $this->authorName]);

        return $dataProvider;
    }
}
