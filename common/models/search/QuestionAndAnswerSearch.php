<?php

namespace common\models\search;

use common\models\QuestionAndAnswer;
use yii\data\ActiveDataProvider;

/**
 * Class QuestionAndAnswerSearch
 * @package common\models\search
 */
class QuestionAndAnswerSearch extends QuestionAndAnswer
{
    /** @var string  */
    public $created_at_date_filter = '=';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['question', 'answer', 'is_enabled', 'created_at'], 'trim'],
            ['created_at_date_filter', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = QuestionAndAnswer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'question', $this->question]);
        $query->andFilterWhere(['like', 'answer', $this->answer]);
        $query->andFilterWhere(['is_enabled' => $this->is_enabled]);

        if (!empty($this->created_at)) {
            $query->andFilterWhere([$this->created_at_date_filter, 'created_at', $this->created_at]);
        }

        return $dataProvider;
    }
}
