<?php

namespace common\models\search;

use common\models\Banner;
use yii\data\ActiveDataProvider;

/**
 * Class BannerSearch
 * @package common\models
 */
class BannerSearch extends Banner
{
    public $category_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['category_id', 'integer'],
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = Banner::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->load($params) || !$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'category_id', $this->category_id]);

        return $dataProvider;
    }
}
