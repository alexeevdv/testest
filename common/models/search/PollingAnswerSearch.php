<?php

namespace common\models\search;

use common\models\PollingAnswer;
use yii\data\ActiveDataProvider;

/**
 * Class PollingAnswerSearch
 * @package common\models\search
 */
class PollingAnswerSearch extends PollingAnswer
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['text', 'trim'],
            ['has_user_variant', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = PollingAnswer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->andFilterWhere(['has_user_variant' => $this->has_user_variant]);

        return $dataProvider;
    }
}
