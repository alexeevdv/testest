<?php

namespace common\models\search;

use common\models\BannerCategory;
use yii\data\ActiveDataProvider;

/**
 * Class BannerCategorySearch
 * @package common\models\search
 */
class BannerCategorySearch extends BannerCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position'], 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = BannerCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'position', $this->position]);

        return $dataProvider;
    }
}
