<?php

namespace common\models\search;

use common\models\Organization;
use yii\data\ActiveDataProvider;

/**
 * Class OrganizationSearch
 * @package common\models\search
 */
class OrganizationSearch extends Organization
{
    /** @var  string */
    public $typeId;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'cityName', 'typeId'], 'trim'],
            ['published_at_date_filter', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = Organization::find();
        $query->joinWith('city city');
        $query->joinWith('types type');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name',
                    'email',
                    'cityName' => [
                        'asc' => ['city.name' => SORT_ASC],
                        'desc' => ['city.name' => SORT_DESC],
                        'default' => SORT_DESC
                    ]
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->alias('organization');
        $query->andFilterWhere(['like', 'organization.name', $this->name]);
        $query->andFilterWhere(['like', 'organization.email', $this->email]);
        $query->andFilterWhere(['type.id' => $this->typeId]);

        $query->andFilterWhere(['like', 'city.name', $this->cityName]);

        return $dataProvider;
    }
}
