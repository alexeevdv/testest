<?php

namespace common\models\search;

use common\models\Article;
use yii\data\ActiveDataProvider;

/**
 * Class ArticleSearch
 * @package common\models
 */
class ArticleSearch extends Article
{
    /** @var  string */
    public $categoryTitle;

    /** @var string  */
    public $published_at_date_filter = '=';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'is_enabled', 'categoryTitle', 'published_at'], 'trim'],
            ['published_at_date_filter', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = Article::find();
        $query->joinWith('category category');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'title',
                    'slug',
                    'is_enabled',
                    'published_at',
                    'categoryTitle' => [
                        'asc' => ['category.title' => SORT_ASC],
                        'desc' => ['category.title' => SORT_DESC],
                        'default' => ['category.title' => SORT_DESC],
                    ]
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->alias('article');
        $query->andFilterWhere(['like', 'article.title', $this->title]);
        $query->andFilterWhere(['like', 'article.slug', $this->slug]);
        $query->andFilterWhere(['article.is_enabled' => $this->is_enabled]);

        if (!empty($this->published_at)) {
            $query->andFilterWhere([$this->published_at_date_filter, 'article.published_at', $this->published_at]);
        }

        $query->andFilterWhere(['like', 'category.title', $this->categoryTitle]);

        return $dataProvider;
    }
}
