<?php

namespace common\models\search;

use common\models\TestQuestion;
use yii\data\ActiveDataProvider;

/**
 * Class TestQuestionSearch
 * @package common\models\search
 */
class TestQuestionSearch extends TestQuestion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'comment', 'is_checkbox'], 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = TestQuestion::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'text', $this->text]);
        $query->andFilterWhere(['like', 'comment', $this->comment]);
        $query->andFilterWhere(['is_checkbox' => $this->is_checkbox]);

        return $dataProvider;
    }
}
