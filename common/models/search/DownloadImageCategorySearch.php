<?php

namespace common\models\search;

use common\models\DownloadImageCategory;
use yii\data\ActiveDataProvider;

/**
 * Class DownloadImageCategorySearch
 * @package common\models\search
 */
class DownloadImageCategorySearch extends DownloadImageCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = DownloadImageCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
