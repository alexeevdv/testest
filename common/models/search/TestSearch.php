<?php

namespace common\models\search;

use common\models\Test;
use yii\data\ActiveDataProvider;

/**
 * Class TestSearch
 * @package common\models\search
 */
class TestSearch extends Test
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_enabled'], 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = Test::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['is_enabled' => $this->is_enabled]);

        return $dataProvider;
    }
}
