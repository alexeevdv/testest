<?php

namespace common\models\search;

use common\models\City;
use yii\data\ActiveDataProvider;

/**
 * Class CitySearch
 * @package common\models\search
 */
class CitySearch extends City
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'regionName'], 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = City::find();

        $query->joinWith('region region');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'regionName' => [
                        'asc' => ['region.name' => SORT_ASC],
                        'desc' => ['region.name' => SORT_DESC],
                        'default' => SORT_DESC,
                    ],
                    'name',
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->alias('city')->andFilterWhere(['like', 'city.name', $this->name]);

        $query->andFilterWhere(['like', 'region.name', $this->regionName]);

        return $dataProvider;
    }
}
