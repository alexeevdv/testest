<?php

namespace common\models\search;

use common\models\FeedbackCategory;
use yii\data\ActiveDataProvider;

/**
 * Class FeedbackCategorySearch
 * @package common\models\search
 */
class FeedbackCategorySearch extends FeedbackCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = FeedbackCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
