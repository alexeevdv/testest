<?php

namespace common\models\search;

use common\models\ForumSection;
use yii\data\ActiveDataProvider;

/**
 * Class ForumMessageSearch
 * @package common\models\search
 */
class ForumSectionSearch extends ForumSection
{
    /** @var string  */
    public $created_at_date_filter = '=';

    /**
     * @var string
     */
    public $parent_title;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'parent_title', 'description', 'created_at', 'parent_id'], 'trim'],
            ['created_at_date_filter', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = ForumSection::find();
        $query->alias('section');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'section.title', $this->title]);
        $query->andFilterWhere(['like', 'description', $this->description]);

        if (!empty($this->created_at)) {
            $query->andFilterWhere([$this->created_at_date_filter, 'created_at', $this->created_at]);
        }

        $query->joinWith('parent parent');
        $query->andFilterWhere(['like', 'parent.title', $this->parent_title]);

        return $dataProvider;
    }
}
