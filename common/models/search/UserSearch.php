<?php

namespace common\models\search;

use common\models\User;
use yii\data\ActiveDataProvider;

/**
 * Class UserSearch
 * @package common\models
 */
class UserSearch extends User
{
    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }
}
