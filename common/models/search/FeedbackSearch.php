<?php

namespace common\models\search;

use common\models\Feedback;
use yii\data\ActiveDataProvider;

/**
 * Class FeedbackSearch
 * @package common\models\search
 */
class FeedbackSearch extends Feedback
{
    /** @var string  */
    public $created_at_date_filter = '=';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'text', 'category_id', 'created_at'], 'trim'],
            ['created_at_date_filter', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = Feedback::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'email', $this->email]);
        $query->andFilterWhere(['like', 'text', $this->text]);

        if (!empty($this->created_at)) {
            $query->andFilterWhere([$this->created_at_date_filter, 'created_at', $this->created_at]);
        }

        $query->andFilterWhere(['category_id' => $this->category_id]);

        return $dataProvider;
    }
}
