<?php

namespace common\models\search;

use common\models\ArticleCategory;
use yii\data\ActiveDataProvider;

/**
 * Class ArticleCategorySearch
 * @package common\models\search
 */
class ArticleCategorySearch extends ArticleCategory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'is_enabled'], 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = ArticleCategory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'slug', $this->slug]);

        $query->andFilterWhere(['is_enabled' => $this->is_enabled]);

        return $dataProvider;
    }
}
