<?php

namespace common\models\search;

use common\models\DownloadImage;
use yii\data\ActiveDataProvider;

/**
 * Class DownloadImageSearch
 * @package common\models\search
 */
class DownloadImageSearch extends DownloadImage
{
    /** @var  string */
    public $categoryName;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'preview', 'categoryName'], 'trim']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = DownloadImage::find();
        $query->joinWith('category category');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name',
                    'preview',
                    'categoryName' => [
                        'asc' => ['category.name' => SORT_ASC],
                        'desc' => ['category.name' => SORT_DESC],
                        'default' => ['category.name' => SORT_DESC],
                    ]
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->alias('download_image');
        $query->andFilterWhere(['like', 'download_image.name', $this->name]);
        $query->andFilterWhere(['like', 'download_image.preview', $this->preview]);

        $query->andFilterWhere(['like', 'category.name', $this->categoryName]);

        return $dataProvider;
    }
}
