<?php

namespace common\models\search;

use common\models\ForumTheme;
use yii\data\ActiveDataProvider;

/**
 * Class ForumMessageSearch
 * @package common\models\search
 */
class ForumThemeSearch extends ForumTheme
{
    /** @var string  */
    public $created_at_date_filter = '=';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'is_closed', 'is_pinned', 'sectionTitle', 'created_at', 'authorName'], 'trim'],
            ['created_at_date_filter', 'safe']
        ];
    }

    /**
     * @param array $params
     * @return ActiveDataProvider
     */
    public function search(array $params = [])
    {
        $query = ForumTheme::find();
        $query->joinWith('section section');
        $query->joinWith('author author');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'title',
                    'created_at',
                    'is_pinned',
                    'is_closed',
                    'sectionTitle' => [
                        'asc' => ['section.title' => SORT_ASC],
                        'desc' => ['section.title' => SORT_DESC],
                        'default' => ['section.title' => SORT_DESC]
                    ],
                    'authorName' => [
                        'asc' => ['author.name' => SORT_ASC],
                        'desc' => ['author.name' => SORT_DESC],
                        'default' => ['author.name' => SORT_DESC]
                    ]
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->alias('theme');
        $query->andFilterWhere(['like', 'theme.title', $this->title]);

        $query->andFilterWhere(['is_closed' => $this->is_closed]);

        $query->andFilterWhere(['is_pinned' => $this->is_pinned]);

        if (!empty($this->created_at)) {
            $query->andFilterWhere([$this->created_at_date_filter, 'created_at', $this->created_at]);
        }

        $query->andFilterWhere(['like', 'section.title', $this->sectionTitle]);

        $query->andFilterWhere(['like', 'author.name', $this->authorName]);

        return $dataProvider;
    }
}
