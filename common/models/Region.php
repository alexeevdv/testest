<?php

namespace common\models;

use common\models\queries\CityQuery;
use common\models\queries\RegionQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Region
 * @package common\models
 *
 * @property int $id
 * @property string $name
 *
 * @property City[] $cities
 */
class Region extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%region}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название региона'
        ];
    }

    /**
     * @return CityQuery
     */
    public function getCities()
    {
        /** @var CityQuery $query */
        $query = $this->hasMany(City::class, ['region_id' => 'id']);
        return $query;
    }

    /**
     * @inheritdoc
     * @return RegionQuery
     */
    public static function find()
    {
        return Yii::createObject(RegionQuery::class, [get_called_class()]);
    }
}
