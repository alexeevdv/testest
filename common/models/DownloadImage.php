<?php

namespace common\models;

use alexeevdv\image\SingleImageUploadBehavior;
use common\models\queries\DownloadImageCategoryQuery;
use common\models\queries\DownloadImageQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class DownloadImage
 * @package common\models
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $file
 * @property string $preview
 *
 * @property DownloadImageCategory $category
 */
class DownloadImage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%download_image}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            [['name', 'category_id'], 'required'],
            ['preview', 'default']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => SingleImageUploadBehavior::class,
                'attributes' => 'file',
                'uploadPath' => '@frontend/web/uploads/images',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
            'name' => 'Название',
            'preview' => 'Превью',
        ];
    }

    /**
     * @return DownloadImageCategoryQuery
     */
    public function getCategory()
    {
        /** @var DownloadImageCategoryQuery $query */
        $query = $this->hasOne(DownloadImageCategory::class, ['id' => 'category_id']);
        return $query;
    }

    /**
     * @inheritdoc
     * @return DownloadImageQuery
     */
    public static function find()
    {
        return Yii::createObject(DownloadImageQuery::class, [get_called_class()]);
    }
}
