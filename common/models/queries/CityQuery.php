<?php

namespace common\models\queries;

use common\models\City;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class CityQuery
 * @package common\models\queries
 *
 * @see City
 */
class CityQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|City
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|City[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param integer $region_id
     * @return static
     */
    public function byRegionId($region_id)
    {
        return $this->andWhere([
            $this->prependWithTableName('region_id') => $region_id,
        ]);
    }
}
