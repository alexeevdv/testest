<?php

namespace common\models\queries;

use common\models\ArticleCategory;
use yii\db\ActiveQuery;

/**
 * Class ArticleCategoryQuery
 * @package common\models\queries
 *
 * @see ArticleCategory
 */
class ArticleCategoryQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|ArticleCategory
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|ArticleCategory[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
