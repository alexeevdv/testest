<?php

namespace common\models\queries;

use common\models\Test;
use yii\db\ActiveQuery;

/**
 * Class TestQuery
 * @package common\models\queries
 *
 * @see Test
 */
class TestQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|null|Test
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|Test[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
