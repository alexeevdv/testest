<?php

namespace common\models\queries;

use common\models\Article;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class ArticleQuery
 * @package common\models\queries
 *
 * @see Article
 */
class ArticleQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|Article
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Article[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param integer $id
     * @return ArticleQuery
     */
    public function byId($id)
    {
        return $this->andWhere([
            $this->prependWithTableName('id') => $id
        ]);
    }

    /**
     * @return ArticleQuery
     */
    public function enabled($status = true)
    {
        return $this->andWhere([
            $this->prependWithTableName('is_enabled') => $status
        ]);
    }
}
