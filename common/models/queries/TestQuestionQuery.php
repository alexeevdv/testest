<?php

namespace common\models\queries;

use common\models\TestQuestion;
use yii\db\ActiveQuery;

/**
 * Class TestQuestionQuery
 * @package common\models\queries
 *
 * @see TestQuestion
 */
class TestQuestionQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|null|TestQuestion
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|TestQuestion[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @return $this
     */
    public function getRight()
    {
        return $this->andWhere(['is_right' => true]);
    }

    /**
     * @return $this
     */
    public function getWrong()
    {
        return $this->andWhere(['is_right' => false]);
    }
}
