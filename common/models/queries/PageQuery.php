<?php

namespace common\models\queries;

use common\models\Page;
use yii\db\ActiveQuery;

/**
 * Class PageQuery
 * @package common\models\query
 *
 * @see Page
 */
class PageQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|Page
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Page[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param integer $id
     * @return PageQuery
     */
    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }

    /**
     * @param bool $status
     * @return static
     */
    public function published($status = true)
    {
        return $this->andWhere([
            'published' => $status,
        ]);
    }
}
