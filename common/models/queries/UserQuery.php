<?php

namespace common\models\queries;

use common\models\User;
use yii\db\ActiveQuery;

/**
 * Class UserQuery
 * @package common\models\queries
 *
 * @see User
 */
class UserQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|User
     */
    public function one($db = null)
    {
        return parent::one($db); // TODO: Change the autogenerated stub
    }

    /**
     * @inheritdoc
     * @return array|User[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
