<?php

namespace common\models\queries;

use common\models\Polling;
use yii\db\ActiveQuery;

/**
 * Class PollingQuery
 * @package common\models\queries
 *
 * @see Polling
 */
class PollingQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|Polling
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Polling[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
