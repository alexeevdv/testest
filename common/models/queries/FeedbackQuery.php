<?php

namespace common\models\queries;

use common\models\Feedback;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class FeedbackQuery
 * @package common\models\queries
 *
 * @see Feedback
 */
class FeedbackQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|Feedback
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Feedback[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
