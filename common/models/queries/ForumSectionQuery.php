<?php

namespace common\models\queries;

use common\models\ForumSection;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class ForumSectionQuery
 * @package common\models\queries
 *
 * @see ForumSection
 */
class ForumSectionQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|ForumSection
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|ForumSection[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param bool $hasParent
     * @return static
     */
    public function hasParent($hasParent = true)
    {
        if ($hasParent) {
            return $this->andWhere(['not', [$this->prependWithTableName('parent_id') => null]]);
        }
        return $this->andWhere([$this->prependWithTableName('parent_id') => null]);
    }

    /**
     * @param integer $id
     * @return static
     */
    public function excludeId($id)
    {
        return $this->andWhere(['!=', $this->prependWithTableName('id'), $id]);
    }
}
