<?php

namespace common\models\queries;

use common\models\Article;
use tests\models\Document;
use yii\db\ActiveQuery;

/**
 * Class ArticleQuery
 * @package common\models\queries
 *
 * @see Document
 */
class DocumentQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|Document
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Document[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
