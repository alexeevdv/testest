<?php

namespace common\models\queries;

use common\models\BannerCategory;
use yii\db\ActiveQuery;

/**
 * Class BannerCategoryQuery
 * @package common\models\queries
 *
 * @see BannerCategory
 */
class BannerCategoryQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|BannerCategory
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|BannerCategory[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
