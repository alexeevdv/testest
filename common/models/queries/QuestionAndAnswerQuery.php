<?php

namespace common\models\queries;

use common\models\QuestionAndAnswer;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class QuestionAndAnswerQuery
 * @package common\models\queries
 *
 * @see QuestionAndAnswer
 */
class QuestionAndAnswerQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|QuestionAndAnswer
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|QuestionAndAnswer[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @return static
     */
    public function byWithoutAnswer()
    {
        return $this->andWhere([
            $this->prependWithTableName('answer') => null,
            $this->prependWithTableName('answer_user_id') => null,
        ]);
    }

    /**
     * @return QuestionAndAnswerQuery
     */
    public function hasAnswer($hasAnswer = true)
    {
        if ($hasAnswer) {
            return $this->andWhere(['not', [$this->prependWithTableName('answer') => null]]);
        }

        return $this->andWhere([
            $this->prependWithTableName('answer') => null
        ]);
    }
}
