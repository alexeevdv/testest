<?php

namespace common\models\queries;

use common\models\Banner;
use yii\db\ActiveQuery;

/**
 * Class BannerQuery
 * @package common\models\queries
 *
 * @see Banner
 */
class BannerQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|Banner
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Banner[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
