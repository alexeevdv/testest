<?php

namespace common\models\queries;

use common\models\FeedbackCategory;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class FeedbackCategoryQuery
 * @package common\models\queries
 *
 * @see FeedbackCategory
 */
class FeedbackCategoryQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|FeedbackCategory
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|FeedbackCategory[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
