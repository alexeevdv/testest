<?php

namespace common\models\queries;

use common\models\DownloadImage;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class DownloadImageQuery
 * @package common\models\queries
 *
 * @see DownloadImage
 */
class DownloadImageQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|DownloadImage
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|DownloadImage[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
