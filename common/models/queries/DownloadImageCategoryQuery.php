<?php

namespace common\models\queries;

use common\models\DownloadImageCategory;
use common\traits\ActiveQueryHelperTrait;
use creocoder\nestedsets\NestedSetsQueryBehavior;
use yii\db\ActiveQuery;

/**
 * Class DownloadImageCategoryQuery
 * @package common\models\queries
 *
 * @see DownloadImageCategory
 */
class DownloadImageCategoryQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|DownloadImageCategory
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|DownloadImageCategory[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            NestedSetsQueryBehavior::class,
        ];
    }

    /**
     * @param integer $id
     * @return static
     */
    public function excludeId($id)
    {
        return $this->andWhere(['!=', $this->prependWithTableName('id'), $id]);
    }
}
