<?php

namespace common\models\queries;

use common\models\Region;
use yii\db\ActiveQuery;

/**
 * Class RegionQuery
 * @package common\models\queries
 *
 * @see Region
 */
class RegionQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|Region
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Region[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
