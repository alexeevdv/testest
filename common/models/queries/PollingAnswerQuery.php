<?php

namespace common\models\queries;

use common\models\PollingAnswer;
use yii\db\ActiveQuery;

/**
 * Class PollingAnswerQuery
 * @package common\models\queries
 *
 * @see PollingAnswer
 */
class PollingAnswerQuery extends ActiveQuery
{
    /**
     * @inheritdoc
     * @return array|null|PollingAnswer
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|PollingAnswer[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
