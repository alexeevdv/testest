<?php

namespace common\models\queries;

use common\models\TestAnswer;
use yii\db\ActiveQuery;

/**
 * Class TesAnswerQuery
 * @package common\models\queries
 *
 * @see TestAnswer
 */
class TestAnswerQuery extends ActiveQuery
{
    /**
     * @param null $db
     * @return array|null|TestAnswer
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @param null $db
     * @return array|TestAnswer[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
