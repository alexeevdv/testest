<?php

namespace common\models\queries;

use common\models\ForumMessage;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class ForumMessageQuery
 * @package common\models\queries
 *
 * @see ForumMessage
 */
class ForumMessageQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|ForumMessage
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|ForumMessage[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
