<?php

namespace common\models\queries;

use common\models\Organization;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class OrganizationQuery
 * @package common\models\queries
 *
 * @see Organization
 */
class OrganizationQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|Organization
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|Organization[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
