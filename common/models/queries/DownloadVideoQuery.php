<?php

namespace common\models\queries;

use common\models\DownloadVideo;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class DownloadVideoQuery
 * @package common\models\queries
 *
 * @see DownloadVideo
 */
class DownloadVideoQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|DownloadVideo
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|DownloadVideo[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
