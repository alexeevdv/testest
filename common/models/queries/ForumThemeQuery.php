<?php

namespace common\models\queries;

use common\models\ForumTheme;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class ForumThemeQuery
 * @package common\models\queries
 *
 * @see ForumTheme
 */
class ForumThemeQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|ForumTheme
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|ForumTheme[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }
}
