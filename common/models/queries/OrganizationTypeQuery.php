<?php

namespace common\models\queries;

use common\models\OrganizationType;
use common\traits\ActiveQueryHelperTrait;
use yii\db\ActiveQuery;

/**
 * Class OrganizationTypeQuery
 * @package common\models\queries
 *
 * @see OrganizationType
 */
class OrganizationTypeQuery extends ActiveQuery
{
    use ActiveQueryHelperTrait;

    /**
     * @inheritdoc
     * @return array|null|OrganizationType
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    /**
     * @inheritdoc
     * @return array|OrganizationType[]
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @param string $name
     * @return static
     */
    public function byName($name)
    {
        return $this->andWhere([
            $this->prependWithTableName('name') => $name
        ]);
    }
}
