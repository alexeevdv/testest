<?php

namespace common\models;

use common\models\queries\ArticleCategoryQuery;
use common\models\queries\ArticleQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class ArticleCategory
 * @package common\models
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property bool $is_enabled
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 *
 * @property Article[] $articles
 */
class ArticleCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'seo_title', 'seo_keywords', 'seo_description'], 'trim'],
            [['title', 'slug'], 'required'],
            ['is_enabled', 'boolean'],
            ['is_enabled', 'default', 'value' => false],

        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название категории',
            'slug' => 'URL-алиас',
            'is_enabled' => 'Отображать',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
        ];
    }

    /**
     * @inheritdoc
     * @return ArticleCategoryQuery
     */
    public static function find()
    {
        return Yii::createObject(ArticleCategoryQuery::class, [get_called_class()]);
    }

    /**
     * @return ArticleQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['category_id' => 'id']);
    }
}
