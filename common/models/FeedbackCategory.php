<?php

namespace common\models;

use common\models\queries\FeedbackCategoryQuery;
use common\models\queries\FeedbackQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class FeedbackCategory
 * @package common\models
 *
 * @property int $id
 * @property string $name
 *
 * @property Feedback[] $feedbacks
 */
class FeedbackCategory extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название категории'
        ];
    }

    /**
     * @return FeedbackQuery
     */
    public function getFeedbacks()
    {
        /** @var FeedbackQuery $query */
        $query = $this->hasMany(Feedback::class, ['category_id' => 'id']);
        return $query;
    }

    /**
     * @inheritdoc
     * @return FeedbackCategoryQuery
     */
    public static function find()
    {
        return Yii::createObject(FeedbackCategoryQuery::class, [get_called_class()]);
    }
}
