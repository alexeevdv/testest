<?php

namespace common\models;

use common\models\queries\DownloadImageCategoryQuery;
use common\models\queries\DownloadImageQuery;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class DownloadImageCategory
 * @package common\models
 *
 * @property int $id
 * @property int $parent_id
 * @property int $tree
 * @property int $lft
 * @property int $rgt
 * @property int $depth
 * @property string $name
 *
 * @property DownloadImage[] $images
 */
class DownloadImageCategory extends ActiveRecord
{
    /**
     * @var integer $parent_id
     */
    public $parent_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%download_image_category}}';
    }

    /**
     * @inheritdoc
     * Для отображения $this->parent_it в представлении при помощи NestedSetsBehavior
     */
    public function afterFind()
    {
        // TODO избавиться от этого. Использовать ModelView для backend
        parent::afterFind();
        if ($this->parents()->all()) {
            $this->parent_id = $this->parents(1)->one()->id;
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['tree', 'default', 'value' => 0],
            ['name', 'trim'],
            ['parent_id', 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'tree' => [
                'class' => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
                'leftAttribute' => 'lft',
                'rightAttribute' => 'rgt',
                'depthAttribute' => 'depth',
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название категории',
            'parent_id' => 'Родительская категория'
        ];
    }

    /**
     * @return DownloadImageQuery
     */
    public function getImages()
    {
        /** @var DownloadImageQuery $query */
        $query = $this->hasMany(DownloadImage::class, ['category_id' => 'id']);
        return $query;
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return DownloadImageCategoryQuery
     */
    public static function find()
    {
        return Yii::createObject(DownloadImageCategoryQuery::class, [get_called_class()]);
    }
}
