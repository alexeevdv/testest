<?php

namespace common\models;

use alexeevdv\image\SingleImageUploadBehavior;
use alexeevdv\file\SingleFileUploadBehavior;
use common\models\queries\DownloadVideoQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class DownloadVideo
 * @package common\models
 *
 * @property integer $id
 * @property string $title
 * @property string $preview
 * @property string $file
 */
class DownloadVideo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%download_video}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SingleFileUploadBehavior::class,
                'attributes' => 'file',
                'uploadPath' => '@frontend/web/uploads/video',
                'validatorOptions' => ['extensions' => 'avi, mp4, txt']
            ],
            [
                'class' => SingleImageUploadBehavior::class,
                'uploadPath' => '@frontend/web/uploads/video',
                'attributes' => 'preview',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'preview' => 'Превью',
            'file' => 'Файл',
        ];
    }

    /**
     * @inheritdoc
     * @return DownloadVideoQuery
     */
    public static function find()
    {
        return Yii::createObject(DownloadVideoQuery::class, [get_called_class()]);
    }
}
