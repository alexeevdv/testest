<?php

namespace common\models;

use common\models\queries\PollingQuery;
use yii\db\ActiveRecord;

/**
 * Class PollingAnswerToUser
 * @package common\models
 *
 * @property string $user_answer
 *
 */
class PollingAnswerToUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polling_answer_to_user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_answer'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Ваш ответ',
        ];
    }

    /**
     * @return PollingQuery
     */
    public function getPolling()
    {
        return $this->hasOne(Polling::class, ['id' => 'polling_id']);
    }
}
