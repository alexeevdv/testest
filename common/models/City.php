<?php

namespace common\models;

use common\models\queries\CityQuery;
use common\models\queries\OrganizationQuery;
use common\models\queries\RegionQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class City
 * @package common\models
 *
 * @property int $id
 * @property int $region_id
 * @property string $name
 * @property string $regionName
 *
 * @property Organization[] $organizations
 * @property Region $region
 */
class City extends ActiveRecord
{
    /** @var  string */
    public $regionName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%city}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'trim'],
            [['name', 'region_id'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Названия городов',
        ];
    }

    /**
     * @return OrganizationQuery
     */
    public function getOrganizations()
    {
        /** @var OrganizationQuery $query */
        $query = $this->hasMany(Organization::class, ['city_id' => 'id']);
        return $query;
    }

    /**
     * @return RegionQuery
     */
    public function getRegion()
    {
        /** @var RegionQuery $query */
        $query = $this->hasOne(Region::class, ['id' => 'region_id']);
        return $query;
    }

    /**
     * @inheritdoc
     * @return CityQuery
     */
    public static function find()
    {
        return Yii::createObject(CityQuery::class, [get_called_class()]);
    }
}
