<?php

namespace common\models;

use common\models\queries\FeedbackCategoryQuery;
use common\models\queries\FeedbackQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class Feedback
 * @package common\models
 *
 * @property int $id
 * @property int $category_id
 * @property int $user_id
 * @property string $name
 * @property string $email
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 *
 * @property FeedbackCategory $category
 */
class Feedback extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%feedback}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'text'], 'trim'],
            [['category_id', 'text', 'email'], 'required'],
            ['email', 'email'],
            ['name', 'required', 'when' => function () {
                return Yii::$app->user->isGuest;
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'ID ползователя',
            'name' => 'ФИО',
            'email' => 'E-mail',
            'text' => 'Текст',
            'created_at' => 'Вопрос задан',
        ];
    }

    /**
     * @return FeedbackCategoryQuery
     */
    public function getCategory()
    {
        /** @var FeedbackCategoryQuery $query */
        $query = $this->hasOne(FeedbackCategory::class, ['id' => 'category_id']);
        return $query;
    }

    /**
     * @inheritdoc
     * @return FeedbackQuery
     */
    public static function find()
    {
        return Yii::createObject(FeedbackQuery::class, [get_called_class()]);
    }
}
