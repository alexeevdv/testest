<?php

namespace common\models;

use common\models\queries\PollingAnswerQuery;
use common\models\queries\PollingQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class PollingAnswer
 * @package common\models
 *
 * @property integer $id
 * @property integer $polling_id
 * @property string $text
 *
 */
class PollingAnswer extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%polling_answer}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'trim'],
            [['polling_id'], 'integer'],
            ['has_user_variant', 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
            'polling_id' => 'Опрос',
            'has_user_variant' => 'Вариант пользователя'
        ];
    }

    /**
     * @inheritdoc
     * @return PollingAnswerQuery
     */
    public static function find()
    {
        return Yii::createObject(PollingAnswerQuery::class, [get_called_class()]);
    }

    /**
     * @return PollingQuery
     */
    public function getPolling()
    {
        return $this->hasOne(Polling::class, ['id' => 'polling_id']);
    }
}
