<?php

namespace common\models;

use alexeevdv\file\SingleFileUploadBehavior;
use common\models\queries\DocumentQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class Document
 * @package common\models
 *
 * @property string $title
 * @property string $author
 * @property string $number
 * @property string $file
 * @property string $start_date
 * @property string $end_date

 */
class Document extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%document}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'author', 'number', 'start_date', 'end_date'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SingleFileUploadBehavior::class,
                'attributes' => 'file',
                'uploadPath' => '@frontend/web/uploads/docs',
                'validatorOptions' => ['extensions' => 'doc, docx, odt, txt'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'number' => 'Номер',
            'author' => 'Автор',
            'file' => 'Файл',
            'start_date' => 'Начало действия',
            'end_date' => 'Окончание действия',
        ];
    }

    /**
     * @inheritdoc
     * @return DocumentQuery
     */
    public static function find()
    {
        return Yii::createObject(DocumentQuery::class, [get_called_class()]);
    }
}
