<?php

namespace common\models;

use common\models\queries\CityQuery;
use common\models\queries\OrganizationQuery;
use common\models\queries\OrganizationTypeQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Organization
 * @package common\models
 *
 * @property int $id
 * @property int $city_id
 * @property string $name
 * @property string $email
 * @property string $site
 * @property string $phones
 * @property string $address
 * @property float $lat
 * @property float $lon
 *
 * @property string $cityName
 *
 * @property OrganizationType[] $types
 * @property City $city
 */
class Organization extends ActiveRecord
{
    /** @var  string */
    public $cityName;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%organization}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'site', 'phones', 'address'], 'trim'],
            [['city_id', 'name'], 'required'],
            [['lat', 'lon'], 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'Город',
            'name' => 'Название',
            'email' => 'E-mail',
            'site' => 'Веб-сайт',
            'phones' => 'Телефоны',
            'address' => 'Адрес',
            'lat' => 'Широта',
            'lon' => 'Долгота',
            'types' => 'Тип организции',
        ];
    }

    /**
     * @return OrganizationTypeQuery
     */
    public function getTypes()
    {
        /** @var OrganizationTypeQuery $query */
        $query = $this
            ->hasMany(OrganizationType::class, ['id' => 'type_id'])
            ->viaTable('organization_to_type', ['organization_id' => 'id']);
        return $query;
    }

    /**
     * @return CityQuery
     */
    public function getCity()
    {
        /** @var CityQuery $query */
        $query = $this->hasOne(City::class, ['id' => 'city_id']);
        return $query;
    }

    /**
     * @inheritdoc
     * @return OrganizationQuery
     */
    public static function find()
    {
        return Yii::createObject(OrganizationQuery::class, [get_called_class()]);
    }
}
