<?php

namespace common\models;

use common\models\queries\BannerCategoryQuery;
use common\models\queries\BannerQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression as DbExpression;

/**
 * Class Banner
 * @package common\models
 */
class Banner extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%banner}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'url'], 'trim'],
            [['image', 'url', 'category_id'], 'required'],
            ['category_id', 'exist', 'targetClass' => BannerCategory::class, 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'image' => 'Изображение',
            'url' => 'URL',
            'category_id' => 'Категория'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new DbExpression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return BannerQuery
     */
    public static function find()
    {
        return Yii::createObject(BannerQuery::class, [get_called_class()]);
    }

    /**
     * @return BannerCategoryQuery
     */
    public function getCategory()
    {
        /** @var BannerCategoryQuery $query */
        $query = $this->hasOne(BannerCategory::class, ['id' => 'category_id']);
        return $query;
    }
}
