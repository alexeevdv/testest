<?php

namespace common\models;

use common\models\queries\TestQuery;
use common\models\queries\TestQuestionQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * Class Test
 * @package common\models
 *
 * @property integer $id
 * @property string $title
 * @property boolean $is_enabled
 *
 * @property TestQuestion[] $questions
 */
class Test extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%test}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['title', 'trim'],
            ['title', 'required'],
            ['is_enabled', 'boolean'],
            ['is_enabled', 'default', 'value' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'is_enabled' => 'Отображать'
        ];
    }

    /**
     * @return TestQuestionQuery
     */
    public function getQuestions()
    {
        /** @var TestQuestionQuery $query */
        $query = $this->hasMany(TestQuestion::class, ['test_id' => 'id']);
        return $query;
    }

    /**
     * @return TestQuery
     */
    public static function find()
    {
        return Yii::createObject(TestQuery::class, [get_called_class()]);
    }
}
