<?php

namespace common\models;

use alexeevdv\image\SingleImageUploadBehavior;
use common\models\queries\ArticleCategoryQuery;
use common\models\queries\ArticleQuery;
use common\validators\SlugValidator;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class Article
 * @package common\models
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $image
 * @property string $text
 * @property string $short_text
 * @property string $slug
 * @property string $published_at
 * @property string $seo_title
 * @property string $seo_keywords
 * @property string $seo_description
 * @property string $created_at
 * @property string $updated_at
 * @property bool $is_enabled
 *
 * @property  ArticleCategory $category
 */
class Article extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text', 'title', 'slug', 'published_at', 'seo_title', 'seo_keywords', 'seo_description'], 'trim'],
            [['text', 'title', 'slug', 'category_id'], 'required'],
            [['published_at'], 'default'],
            ['slug', SlugValidator::class],
            ['slug', 'unique'],
            [['seo_title', 'seo_keywords', 'seo_description'], 'default'],
            ['is_enabled', 'boolean'],
            ['is_enabled', 'default', 'value' => false]
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => SingleImageUploadBehavior::class,
                'attributes' => 'image',
                'uploadPath' => '@frontend/web/uploads/articles',
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'slug' => 'URL алиас',
            'image' => 'Изображение',
            'text' => 'Текст',
            'short_text' => 'Лид',
            'seo_title' => 'SEO Title',
            'seo_keywords' => 'SEO Keywords',
            'seo_description' => 'SEO Description',
            'published_at' => 'Дата публикации',
            'is_enabled' => 'Отображать'
        ];
    }

    /**
     * @inheritdoc
     * @return ArticleQuery
     */
    public static function find()
    {
        return Yii::createObject(ArticleQuery::class, [get_called_class()]);
    }

    /**
     * @return ArticleCategoryQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ArticleCategory::class, ['id' => 'category_id']);
    }
}
