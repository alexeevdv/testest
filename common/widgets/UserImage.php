<?php

namespace common\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class UserImage
 * @package common\widgets
 */
class UserImage extends Widget
{
    public $model;
    public $width = 120;
    public $height = 120;
    public $htmlOptions = [];

    /**
     * @return string
     */
    public function run()
    {
        $src = 'https://placeholdit.imgix.net/~text?txt=No+photo&w=' . $this->width . '&h=' . $this->height . '&fm=png&txttrack=1';
        return Html::img($src, $this->htmlOptions);
    }
}
