<?php

namespace common\widgets;

use kartik\datecontrol\DateControl;

/**
 * @license https://opensource.org/licenses/mit-license.php MIT License
 */
class DatePickerWidget extends \yii\widgets\InputWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        echo DateControl::widget([
            'model' => $this->model,
            'attribute' => $this->attribute,
            'type' => 'date',
        ]);
    }
}
