<?php

namespace common\traits;

use yii\db\ActiveQuery;

/**
 * Class ActiveQueryHelper
 * @package app\traits
 */
trait ActiveQueryHelperTrait
{
    /**
     * @param string $name
     * @param mixed $value
     * @return static
     */
    public function byAttribute($name, $value)
    {
        return $this->andWhere([
            $this->prependWithTableName($name) => $value,
        ]);
    }

    /**
     * @param string $field
     * @return string
     */
    public function prependWithTableName($field)
    {
        /** @var ActiveQuery $this */
        list($table, $alias) = $this->getTableNameAndAlias();
        return $alias . '.[[' . $field . ']]';
    }

    /**
     * Forced to copy/paste this method from \yii\db\ActiveQuery because for some strange reason it is private
     * @return array
     */
    private function getTableNameAndAlias()
    {
        if (empty($this->from)) {
            $tableName = $this->getPrimaryTableName();
        } else {
            $tableName = '';
            foreach ($this->from as $alias => $tableName) {
                if (is_string($alias)) {
                    return [$tableName, $alias];
                } else {
                    break;
                }
            }
        }

        if (preg_match('/^(.*?)\s+({{\w+}}|\w+)$/', $tableName, $matches)) {
            $alias = $matches[2];
        } else {
            $alias = $tableName;
        }

        return [$tableName, $alias];
    }
}
