<?php

namespace common\routing;

use yii\base\Object;
use yii\web\UrlRuleInterface;

/**
 * Class BaseUrlRule
 * @package common\routing
 */
class BaseUrlRule extends Object implements UrlRuleInterface
{
    /**
     * @inheritdoc
     */
    public function parseRequest($manager, $request)
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function createUrl($manager, $route, $params)
    {
        return false;
    }

    /**
     * Appends given array of params to url as get params
     * @param string $url
     * @param array $params
     * @return string
     */
    protected function appendParams($url, $params)
    {
        if (count($params)) {
            $url .= '?' . http_build_query($params);
        }
        return $url;
    }
}
