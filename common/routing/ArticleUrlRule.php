<?php

namespace common\routing;

use common\models\Article;
use yii\base\InvalidConfigException;

/**
 * Class ArticleUrlRule
 * @package common\routing
 */
class ArticleUrlRule extends BaseUrlRule
{
    /**
     * @inheritdoc
     */
    public function parseRequest($manager, $request)
    {
        if (strpos($request->pathInfo, 'article/') !== 0) {
            return false;
        }

        $parts = explode('/', $request->pathInfo);
        if (count($parts) == 2) {
            $articleSlug = $parts[1];

            $article = Article::find()
                ->andWhere([
                    'slug' => $articleSlug,
                ])
                ->one();

            if (!$article) {
                return false;
            }

            return ['article/view', ['id' => $article->id]];
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'article/view') {
            if (!isset($params['id'])) {
                throw new InvalidConfigException('`id` params is required');
            }

            $article= Article::findOne($params['id']);
            unset($params['id']);
            if (!$article) {
                return false;
            }

            return $this->appendParams('article/' . $article->slug, $params);
        }

        return false;
    }
}
