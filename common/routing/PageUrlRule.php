<?php

namespace common\routing;

use common\models\Page;
use yii\base\InvalidConfigException;

/**
 * Class PageUrlRule
 * @package common\routing
 */
class PageUrlRule extends BaseUrlRule
{
    /**
     * @inheritdoc
     */
    public function parseRequest($manager, $request)
    {
        $pathInfo = $request->pathInfo;
        // TODO published() scope
        $page = Page::find()
            ->published()
            ->andWhere([
                'slug' => $pathInfo,
            ])
            ->one();

        if (!$page) {
            return false;
        }

        return ['page/view', ['id' => $page->id]];
    }

    /**
     * @inheritdoc
     */
    public function createUrl($manager, $route, $params)
    {
        if ($route === 'page/view') {
            if (isset($params['id'])) {
                $page = Page::findOne($params['id']);
                unset($params['id']);
            } elseif (isset($params['slug'])) {
                $page = Page::findOne(['slug' => $params['slug']]);
                unset($params['slug']);
            } else {
                throw new InvalidConfigException('`id` or `slug` is required');
            }

            if (!$page) {
                return false;
            }
            /** @var Page $page */
            return $this->appendParams($page->slug, $params);
        }

        return false;
    }
}
