<?php

namespace frontend\controllers;

use common\models\Test;
use common\models\TestAnswer;
use frontend\models\TestQuestionForm;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class TestController
 * @package frontend\controllers
 */
class TestController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $model = Test::find()->all();

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        $questions = TestQuestionForm::findAll(['test_id' => $id]);
        $wrongAnswers = [];

        if (Model::loadMultiple($questions, Yii::$app->request->post()) && Model::validateMultiple($questions)) {
            foreach ($questions as $question) {
                $selectedAnswer = TestAnswer::findOne($question->answer_id);
                if ($selectedAnswer->is_right) {
                    echo 'Этот ответ правильный <br>';
                } else {
                    echo 'Неправильный ответ <br>';
                    $wrongAnswers[] = $question->id;
                }
            }
        }

        return $this->render('view', [
            'questions' => $questions,
            'wrongAnswers' => $wrongAnswers
        ]);
    }

    /**
     * @param integer $id
     * @return Test
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Test::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
