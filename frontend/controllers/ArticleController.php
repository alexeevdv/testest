<?php

namespace frontend\controllers;

use common\models\Article;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class ArticleController
 * @package frontend\controllers
 */
class ArticleController extends Controller
{
    /**
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * @param integer $id
     * @return array|Article|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Article::find()
            ->byId($id)
            ->enabled()
            ->one();
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
