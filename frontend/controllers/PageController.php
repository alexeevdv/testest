<?php

namespace frontend\controllers;

use common\models\Page;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class PageController
 * @package frontend\controllers
 */
class PageController extends Controller
{
    /**
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return array|Page|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Page::find()
            ->byId($id)
            ->published()
            ->one();
        if (!$model) {
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
