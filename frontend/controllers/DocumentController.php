<?php

namespace frontend\controllers;

use common\models\Document;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class DocumentController
 * @package frontend\controllers
 */
class DocumentController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $documents = Document::find()->all();
        return $this->render('index', [
            'documents' => $documents
        ]);
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * @param integer $id
     * @return Document
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Document::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
