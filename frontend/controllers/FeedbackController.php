<?php

namespace frontend\controllers;

use common\models\Feedback;
use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class FeedbackController
 * @package frontend\controllers
 */
class FeedbackController extends Controller
{
    /**
     * @return string|Response
     */
    public function actionIndex()
    {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!Yii::$app->user->isGuest) {
                $model->user_id = Yii::$app->user->getId();
            }
            $model->save();
            Yii::$app->session->addFlash('success', 'Ваш вопрос успешно отправлен');
            return $this->redirect(['site/index']);
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }
}
