<?php

namespace frontend\controllers;

use common\models\DownloadImage;
use common\models\DownloadVideo;
use yii\web\Controller;

/**
 * Class DownloadController
 * @package frontend\controllers
 */
class DownloadController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $images = DownloadImage::find()->all();
        $videos = DownloadVideo::find()->all();

        return $this->render('index', [
            'images' => $images,
            'videos' => $videos
        ]);
    }
}
