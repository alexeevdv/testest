<?php

namespace frontend\controllers;

use common\models\OrganizationType;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class OrganizationController
 * @package frontend\controllers
 */
class OrganizationController extends Controller
{
    /**
     * @return string
     */
    public function actionTest($type_id)
    {
        $models = $this->findModel($type_id)->organizations;

        return$this->render('index', [
            'models' => $models
        ]);
    }

    /**
     * @param integer $id
     * @return OrganizationType
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = OrganizationType::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
