<?php

namespace frontend\controllers;

use common\models\QuestionAndAnswer;
use yii\web\Controller;

/**
 * Class QuestionAndAnswerController
 * @package frontend\controllers
 */
class QuestionAndAnswerController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        // TODO лучше использовать тут DataProvider
        $models = QuestionAndAnswer::find()
            ->hasAnswer()
            ->all();

        return $this->render('index', [
            'models' => $models
        ]);
    }
}
