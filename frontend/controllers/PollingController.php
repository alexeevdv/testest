<?php

namespace frontend\controllers;

use common\models\Polling;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class PollingController
 * @package frontend\controllers
 */
class PollingController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $query = Polling::find();
        $dataProvider = new ActiveDataProvider([
           'query' => $query
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param integer $id
     * @return string
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * @param integer $id
     * @return Polling
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Polling::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
