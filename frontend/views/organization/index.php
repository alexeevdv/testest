<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/** @var \common\models\Organization[] $models */
foreach ($models as $organization) {
    echo $organization->name . '<br>';
}

$array = ArrayHelper::map($models, 'lat', 'lon');

?>

<div id="map" style="width: auto; height: 400px"></div>

<?php
    $this->registerJS('
        ymaps.ready(init);
        var myMap;
        
        var points = ' . Json::encode($array) . ';

        function init(){   
            myMap = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 3
            });
            
            for (var lat in points) {
                var lon = points[lat];
                
                myPlacemark = new ymaps.Placemark([lat, lon], {});
                myMap.geoObjects.add(myPlacemark);
            }
        }
    ');
?>
