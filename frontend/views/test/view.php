<?php

use frontend\models\TestQuestionForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin([
]);
/** @var TestQuestionForm[] $questions */
?>
<div class="box box-primary">
    <div class="box-body">
<?php
$count = 0;
foreach ($questions as $index => $question) {
    $count++;
    if (empty($wrongAnswers)) {
        echo $form->field($question, "[$index]answer_id")
            ->radioList(ArrayHelper::map($question->answers, 'id', 'text'), ['encode' => false])
            ->label("$count. $question->text?");
    } else {
        if (in_array($question->id, $wrongAnswers)) {
            echo $form->field($question, "[$index]answer_id")
                ->radioList(ArrayHelper::map($question->answers, 'id', 'text'), ['encode' => false])
                ->label("$count. $question->text?");

            echo '<div>' . $question->comment . '</div><br>';
        }
    }
}
?>
</div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>

<?php $form->end(); ?>

