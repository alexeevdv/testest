<?php

$this->title = empty($model->seo_title) ? $model->name : $model->seo_title;

$this->params['keywords'] = $model->seo_keywords;
$this->params['description'] = $model->seo_description;

?>
<div class="row">
    <div class="col-md-12">
        <?=$model->text?>
    </div>
</div>

