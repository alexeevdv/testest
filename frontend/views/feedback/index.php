<?php

use alexeevdv\recaptcha\RecaptchaWidget;
use common\models\Feedback;
use common\models\FeedbackCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var Feedback $model
 */

$this->title = 'Обратная связь';

?>
<div class="box box-primary">
    <div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form
        ->field($model, 'category_id')
        ->dropDownList(ArrayHelper::map(FeedbackCategory::find()->all(), 'id', 'name'))
    ?>

    <?= $form
        ->field($model, 'text')
        ->textarea()
    ?>

    <?= $form
        ->field($model, 'recaptcha')
        ->widget(RecaptchaWidget::class)
    ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
    <?php $form->end(); ?>
</div>
