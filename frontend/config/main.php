<?php

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'components' => [
        'recaptcha' => [
            'class' => 'alexeevdv\recaptcha\Recaptcha',
        ],
        'backendUrlManager' => require(Yii::getAlias('@backend') . '/config/url-manager.php'),
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => require(dirname(__FILE__) . '/url-manager.php'),
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
    ],
    'controllerNamespace' => 'frontend\controllers',
];
