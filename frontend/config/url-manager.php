<?php

return [
    'class' => 'yii\web\UrlManager',
    'baseUrl' => '',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        '/' => 'site/index',
        ['class' => 'common\routing\PageUrlRule'],
        ['class' => 'common\routing\ArticleUrlRule'],
    ],
];
