<?php

namespace frontend\models;

use common\models\TestAnswer;
use common\models\TestQuestion;

/**
 * Class TestQuestionForm
 * @package frontend\models
 */
class TestQuestionForm extends TestQuestion
{
    /** @var  integer $answer_id */
    public $answer_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['answer_id', 'required'];
        $rules[] = ['answer_id', 'exist', 'targetClass' => TestAnswer::className(), 'targetAttribute' => 'id'];
        return $rules;
    }
}
