<?php

namespace Deployer;

require 'vendor/deployer/deployer/recipe/common.php';
require 'vendor/deployer/recipes/recipe/slack.php';

// Configuration
set('keep_releases', 2);

set('ssh_type', 'native');
set('ssh_multiplexing', true);

set('repository', 'git@github.com:ejen/o-spide.ru.git');
set('slack', [
    'channel' => 'o-spide_ru',
    'token' => 'xoxp-189213031697-189213031761-200423709443-e6ec059604f60669cd4c71249c6b74a7',
    'team'  => 'ejen',
]);

add('shared_files', [
    'frontend/config/main-local.php',
    'frontend/config/params-local.php',
    'backend/config/main-local.php',
    'backend/config/params-local.php',
    'console/config/main-local.php',
    'console/config/params-local.php',
    'common/config/main-local.php',
    'common/config/params-local.php',
]);
add('shared_dirs', [
    'frontend/web/uploads',
]);
add('writable_dirs', []);


// Servers

host('o-spide.ejen.ru')
    ->stage('staging')
    ->user('o-spide.ejen.ru')
    ->identityFile('~/.ssh/id_rsa.pub')
    ->set('deploy_path', '/home/o-spide.ejen.ru')
    ->set('branch', 'develop')
    ->set('yii_env', 'Staging');

host('???')
    ->stage('production')
    ->user('????')
    ->identityFile('~/.ssh/id_rsa.pub')
    ->set('deploy_path', '????')
    ->set('branch', 'develop')
    ->set('yii_env', 'Production');

task('deploy:init', function () {
    run('{{bin/php}} {{release_path}}/init --env={{yii_env}} --overwrite=y');
});

task('deploy:migrate', function () {
    run('{{bin/php}} {{release_path}}/yii migrate up --interactive=0');
});

// Tasks
task('deploy', [
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'deploy:init',
    'deploy:migrate',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

after('deploy', 'deploy:slack');
after('deploy:failed', 'deploy:unlock');
