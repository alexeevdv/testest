<?php

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'components' => [
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'frontendUrlManager' => require(Yii::getAlias('@frontend') . '/config/url-manager.php'),
        'urlManager' => require(dirname(__FILE__) . '/url-manager.php'),
        'user' => [
            'identityClass' => 'common\models\User',
            'loginUrl' => ['/user/login'],
            'enableAutoLogin' => true,
        ],
    ],
    'controllerNamespace' => 'backend\controllers',
];
