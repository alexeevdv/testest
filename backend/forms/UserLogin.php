<?php

namespace backend\forms;

use common\models\User;
use yii\base\Model;

/**
 * Class UserLogin
 * @package backend\forms
 */
class UserLogin extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $password;

    /**
     * @var bool
     */
    public $rememberMe = true;

    /**
     * @var User
     */
    protected $_user = false;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'email'],
            ['password', 'required'],
            ['password', 'validatePassword'],
            ['rememberMe', 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => 'E-mail',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        ];
    }

    /**
     *
     */
    public function validatePassword()
    {
        $user = $this->getUser();
        if (!$user) {
            return $this->addError('email', 'Пользователь не найден');
        }

        if (!$user->validatePassword($this->password)) {
            return $this->addError('password', 'Пароль указан не верно');
        }

        // @todo Проверка прав доступа в админку
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findOne(['email' => $this->email]);
        }

        return $this->_user;
    }
}
