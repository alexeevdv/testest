<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\Banner;
use common\models\search\BannerSearch;
use yii\web\Controller;

/**
 * Class BannerController
 * @package backend\controllers
 */
class BannerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => Banner::class,
                'searchModelClass' => BannerSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Banner::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Banner::class,
            ],
        ];
    }
}
