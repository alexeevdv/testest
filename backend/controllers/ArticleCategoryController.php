<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use backend\controllers\actions\UpdateAction;
use common\models\ArticleCategory;
use common\models\search\ArticleCategorySearch;
use yii\web\Controller;

/**
 * Class ArticleCategoryController
 * @package backend\controllers
 */
class ArticleCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => ArticleCategorySearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => ArticleCategory::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => ArticleCategory::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => ArticleCategory::class,
                'attribute' => 'is_enabled',
            ]
        ];
    }
}
