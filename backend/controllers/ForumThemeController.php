<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use common\models\ForumSection;
use common\models\ForumTheme;
use common\models\search\ForumThemeSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ForumThemeController
 * @package backend\controllers
 */
class ForumThemeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => ForumThemeSearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => ForumTheme::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => ForumTheme::class,
                'attribute' => 'is_closed',
            ]
        ];
    }

    /**
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new ForumTheme();
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Данные успешно сохранены');
            return $this->redirect(['index']);
        }

        $sections = ForumSection::find()
            ->hasParent(false)
            ->all();

        return $this->render('update', [
            'model' => $model,
            'sections' => $sections
        ]);
    }

    /**
     * @param integer $id
     * @return ForumTheme
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = ForumTheme::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
