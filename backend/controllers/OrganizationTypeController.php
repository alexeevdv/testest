<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\OrganizationType;
use yii\web\Controller;

/**
 * Class OrganizationTypeController
 * @package backend\controllers
 */
class OrganizationTypeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => OrganizationType::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => OrganizationType::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => OrganizationType::class,
            ],
        ];
    }
}
