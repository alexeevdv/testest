<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\ToggleAction;
use common\models\TestAnswer;
use common\models\TestQuestion;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class TestAnswerController
 * @package backend\controllers
 */
class TestAnswerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => TestQuestion::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => TestAnswer::class,
                'attribute' => 'is_right',
            ],
        ];
    }

    /**
     * @param int $question_id
     * @return string
     */
    public function actionIndex($question_id)
    {
        $question =$this->findQuestion($question_id);
        $dataProvider = new ActiveDataProvider([
            'query' => $question->getAnswers(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'question' => $question
        ]);
    }

    /**
     * @param int $id
     * @param int $question_id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $question_id = 0)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new TestAnswer();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($question_id) {
                $model->question_id = $question_id;
            }
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Данные успешно сохранены');
                return $this->redirect(['test-answer/index', 'question_id' => $question_id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $question_id
     * @return TestQuestion
     * @throws NotFoundHttpException
     */
    public function findQuestion($question_id)
    {
        $model = TestQuestion::findOne($question_id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }

    /**
     * @param integer $id
     * @return TestAnswer
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = TestAnswer::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
