<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\Document;
use common\models\search\DocumentSearch;
use yii\web\Controller;

/**
 * Class DocumentController
 * @package backend\controllers
 */
class DocumentController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => DocumentSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Document::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Document::class,
            ],
        ];
    }
}
