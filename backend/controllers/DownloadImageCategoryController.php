<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use common\models\DownloadImageCategory;
use common\models\search\DownloadImageCategorySearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class DownloadImageCategoryController
 * @package backend\controllers
 */
class DownloadImageCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => DownloadImageCategorySearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => DownloadImageCategory::class,
            ],
        ];
    }

    /**
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new DownloadImageCategory;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if (!$model->parent_id) {
                $model->makeRoot();
            } else {
                $parent = DownloadImageCategory::findOne($model->parent_id);
                $model->appendTo($parent);
            }
            Yii::$app->session->getFlash('success', 'Данные успещно сохранены');
            return $this->redirect(['download-image-category/index']);
        }

        $categoriesToShow = ArrayHelper::map(
            DownloadImageCategory::find()
                ->excludeId($model->id)
                ->all(),
            'id',
            'name'
        );


        return $this->render('update', [
            'model' => $model,
            'categoriesToShow' => $categoriesToShow
        ]);
    }

    /**
     * @param integer $id
     * @return DownloadImageCategory
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = DownloadImageCategory::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
