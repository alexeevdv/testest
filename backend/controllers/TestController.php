<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use backend\controllers\actions\UpdateAction;
use common\models\search\TestSearch;
use common\models\Test;
use yii\web\Controller;

/**
 * Class TestController
 * @package backend\controllers
 */
class TestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => TestSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Test::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Test::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => Test::class,
                'attribute' => 'is_enabled'
            ]
        ];
    }
}
