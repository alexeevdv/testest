<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\FeedbackCategory;
use common\models\search\FeedbackCategorySearch;
use yii\web\Controller;

/**
 * Class FeedbackCategoryController
 * @package backend\controllers
 */
class FeedbackCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => FeedbackCategorySearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => FeedbackCategory::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => FeedbackCategory::class,
            ],
        ];
    }
}
