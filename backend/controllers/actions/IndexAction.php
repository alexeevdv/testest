<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class IndexAction
 * @package backend\controllers\actions
 */
class IndexAction extends Action
{
    /**
     * @var ActiveQuery
     */
    public $query;

    /**
     * @var string
     */
    public $modelClass;

    /**
     * @var string
     */
    public $searchModelClass;

    /**
     * @var string
     */
    public $view = 'index';

    /**
     * @return string
     */
    public function run()
    {
        if ($this->searchModelClass) {
            $searchModel = new $this->searchModelClass;
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->controller->render($this->view, [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => $this->modelClass::find(),
            ]);
            return $this->controller->render($this->view, [
                'dataProvider' => $dataProvider
            ]);
        }
    }
}
