<?php

namespace backend\controllers\actions;

use backend\behaviors\MultipleImageUploadBehavior;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * Class ImageDeleteAction
 * @package backend\controllers\actions
 */
class ImageDeleteAction extends Action
{
    /**
     * @var string
     */
    public $modelClass;

    /**
     * @var string
     */
    public $relation;

    /**
     * @inheritdoc
     */
    public function init()
    {
        if (!$this->modelClass) {
            throw new InvalidConfigException('`modelClass` is required.');
        }
        if (!$this->relation) {
            throw new InvalidConfigException('`relation` is required.');
        }
        if (!$this->getMultipleImageUploadBehavior()) {
            throw new InvalidConfigException('Model should have MultipleImageUploadBehavior behavior.');
        }
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $behavior = $this->getMultipleImageUploadBehavior();
        $modelClass = $behavior->owner->getRelation($this->relation)->modelClass;
        /** @var ActiveRecord $model */
        $model = $modelClass::findOne(Yii::$app->request->post('key'));
        if (!$model) {
            throw new NotFoundHttpException;
        }
        $model->delete();

        return $this->controller->asJson([
        ]);
    }

    /**
     * @return MultipleImageUploadBehavior|null
     */
    protected function getMultipleImageUploadBehavior()
    {
        $modelClass = $this->modelClass;
        $model = new $modelClass;
        foreach ($model->getBehaviors() as $behavior) {
            if ($behavior instanceof MultipleImageUploadBehavior) {
                if ($behavior->relation == $this->relation) {
                    return $behavior;
                }
            }
        }
        return null;
    }
}
