<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\web\Response;

/**
 * Class DeleteAction
 * @package backend\controllers\actions
 */
class DeleteAction extends Action
{
    /**
     * @var string
     */
    public $modelClass;

    /**
     * @return Response
     */
    public function run()
    {
        $modelClass = $this->modelClass;
        $models = $modelClass::find()->andWhere(['id' => Yii::$app->request->post('ids')])->all();

        foreach ($models as $model) {
            $model->delete();
        }

        Yii::$app->session->addFlash('success', 'Выбранные вами элементы успешно удалены');

        return $this->controller->redirect(Yii::$app->request->referrer);
    }
}
