<?php

namespace backend\controllers\actions;

use yii\base\Action;

/**
 * Class ToggleAction
 * @package backend\controllers\actions
 */
class ToggleAction extends Action
{
    /**
     * @var string
     */
    public $modelClass;

    /**
     * @var string
     */
    public $attribute = 'published';

    /**
     * @param int $id
     */
    public function run($id)
    {
        $className = $this->modelClass;
        $model = $className::findOne($id);
        $model->{$this->attribute} = !$model->{$this->attribute};
        $model->save(false, [$this->attribute]);
    }
}
