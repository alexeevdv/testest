<?php

namespace backend\controllers\actions;

use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UpdateAction
 * @package backend\controllers\actions
 */
class UpdateAction extends Action
{
    /**
     * @var string
     */
    public $modelClass;

    /**
     * @var string
     */
    public $view = 'update';

    /**
     * @var string
     */
    public $successMessage = 'Данные успешно сохранены';

    /**
     * @param int $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function run($id)
    {
        $modelClass = $this->modelClass;
        if ($id) {
            $model = $modelClass::findOne($id);
            if (!$model) {
                throw new NotFoundHttpException;
            }
        } else {
            $model = new $modelClass;
        }

        /** @var ActiveRecord $model */
        if ($model->load(Yii::$app->request->post())) {
            if (Yii::$app->request->isPjax) {
            } else {
                if ($model->save()) {
                    Yii::$app->session->addFlash('success', $this->successMessage);
                    $this->controller->goBack();
                }
            }
        } else {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->controller->render($this->view, [
            'model' => $model,
        ]);
    }
}
