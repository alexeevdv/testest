<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use backend\controllers\actions\UpdateAction;
use common\models\Page;
use common\models\search\PageSearch;
use yii\web\Controller;

/**
 * Class PageController
 * @package backend\controllers
 */
class PageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => PageSearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Page::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Page::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => Page::class,
            ],
        ];
    }
}
