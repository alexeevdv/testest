<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use backend\controllers\actions\UpdateAction;
use common\models\Article;
use common\models\search\ArticleSearch;
use yii\web\Controller;

/**
 * Class ArticleController
 * @package backend\controllers
 */
class ArticleController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => ArticleSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Article::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Article::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => Article::class,
                'attribute' => 'is_enabled'
            ]
        ];
    }
}
