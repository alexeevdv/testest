<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\City;
use common\models\search\CitySearch;
use yii\web\Controller;
use yii\web\Response;

/**
 * Class CityController
 * @package backend\controllers
 */
class CityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => CitySearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => City::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => City::class,
            ],
        ];
    }

    /**
     * @return Response
     */
    public function actionDependsOnRegionList()
    {
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $region_id = $parents[0];
                $cities = City::find()
                    ->byRegionId($region_id)
                    ->all();
                foreach ($cities as $city) {
                    $out[] = [
                        'id' => $city->id,
                        'name' => $city->name
                    ];
                }
                return $this->asJson([
                    'output' => $out,
                    'selected' => ''
                ]);
            }
        }
        $this->asJson([
            'output' => [],
            'selected' => ''
        ]);
    }
}
