<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\Region;
use common\models\search\RegionSearch;
use yii\web\Controller;

/**
 * Class RegionController
 * @package backend\controllers
 */
class RegionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => RegionSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Region::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Region::class,
            ],
        ];
    }
}
