<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\DownloadVideo;
use common\models\search\DownloadVideoSearch;
use yii\web\Controller;

/**
 * Class DownloadVideoController
 * @package backend\controllers
 */
class DownloadVideoController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => DownloadVideoSearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => DownloadVideo::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => DownloadVideo::class,
            ],
        ];
    }
}
