<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\BannerCategory;
use common\models\search\BannerCategorySearch;
use yii\web\Controller;

/**
 * Class BannerCategoryController
 * @package backend\controllers
 */
class BannerCategoryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => BannerCategorySearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => BannerCategory::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => BannerCategory::class,
            ],
        ];
    }
}
