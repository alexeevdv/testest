<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use common\models\ForumSection;
use common\models\search\ForumSectionSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ForumSectionController
 * @package backend\controllers
 */
class ForumSectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => ForumSectionSearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => ForumSection::class,
            ],
        ];
    }

    /**
     * @param integer $id
     * @return string|Response
     */
    public function actionUpdate($id)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new ForumSection;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Данные успешно сохранены');
            return $this->redirect(['index']);
        }

        $parentsQuery = ForumSection::find()
            ->hasParent(false);

        if (!$model->isNewRecord) {
            $parentsQuery->excludeId($model->primaryKey);
        }

        return $this->render('update', [
            'model' => $model,
            'parents' => $parentsQuery->all(),
        ]);
    }

    /**
     * @param integer $id
     * @return ForumSection
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = ForumSection::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
