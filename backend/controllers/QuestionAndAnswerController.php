<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use backend\controllers\actions\UpdateAction;
use common\models\QuestionAndAnswer;
use common\models\search\QuestionAndAnswerSearch;
use yii\web\Controller;

/**
 * Class QuestionAndAnswerController
 * @package backend\controllers
 */
class QuestionAndAnswerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => QuestionAndAnswerSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => QuestionAndAnswer::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => QuestionAndAnswer::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => QuestionAndAnswer::class,
                'attribute' => 'is_enabled',
            ],
        ];
    }
}
