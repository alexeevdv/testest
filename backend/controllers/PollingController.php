<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use backend\controllers\actions\UpdateAction;
use common\models\Polling;
use common\models\search\PollingSearch;
use yii\web\Controller;

/**
 * Class PollingController
 * @package backend\controllers
 */
class PollingController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => PollingSearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Polling::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => Polling::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => Polling::class,
                'attribute' => 'is_active'
            ],
        ];
    }
}
