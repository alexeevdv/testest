<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\UpdateAction;
use common\models\DownloadImage;
use common\models\search\DownloadImageSearch;
use yii\web\Controller;

/**
 * Class DownloadImageController
 * @package backend\controllers
 */
class DownloadImageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => DownloadImageSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => DownloadImage::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => DownloadImage::class,
            ],
        ];
    }
}
