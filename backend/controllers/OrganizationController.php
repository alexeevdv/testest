<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use common\models\Organization;
use common\models\OrganizationType;
use common\models\search\OrganizationSearch;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class OrganizationController
 * @package backend\controllers
 */
class OrganizationController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => OrganizationSearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Organization::class,
            ],
        ];
    }

    /**
     * @param integer $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new Organization;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // TODO весь этот блок явно может быть легко вынесен в отдельное поведение
            $model->unlinkAll('types', true);
            $types = ArrayHelper::getValue(Yii::$app->request->post('Organization'), 'types', []);
            if ($types) {
                foreach ($types as $type_id) {
                    $type = OrganizationType::findOne($type_id);
                    $model->link('types', $type);
                }
            }

            Yii::$app->getSession()->addFlash('success', 'Данные успешно сохранены');
            return $this->redirect(['index']);
        }

        $types = OrganizationType::find()->all();

        return $this->render('update', [
            'model' => $model,
            'types' => $types,
        ]);
    }

    /**
     * @param integer $id
     * @return Organization
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Organization::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
