<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\ToggleAction;
use common\models\Polling;
use common\models\PollingAnswer;
use common\models\search\PollingAnswerSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class PollingAnswerController
 * @package backend\controllers
 */
class PollingAnswerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => PollingAnswer::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => PollingAnswer::class,
                'attribute' => 'is_active'
            ],
        ];
    }

    /**
     * @param int $polling_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($polling_id)
    {
        $polling = $this->findPollingModel($polling_id);

        $dataProvider = new ActiveDataProvider([
            'query' => $polling->getAnswers(),
        ]);

        $searchModel = new PollingAnswerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'polling' => $polling,
        ]);
    }

    /**
     * @param int $id
     * @param int $polling_id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $polling_id = null)
    {
        if ($id) {
            $model = $this->findModel($id);
            $polling = $model->polling;
        } else {
            $polling = $this->findPollingModel($polling_id);
            $model = new PollingAnswer([
                'polling_id' => $polling->primaryKey,
            ]);
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Данные сохранены');
                $this->goBack();
            }
        } else {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('update', [
            'model' => $model,
            'polling' => $polling,
        ]);
    }

    /**
     * @param int $id
     * @return PollingAnswer
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = PollingAnswer::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException;
        }
        return $model;
    }

    /**
     * @param int $id
     * @return Polling
     * @throws NotFoundHttpException
     */
    protected function findPollingModel($id)
    {
        $model = Polling::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException;
        }
        return $model;
    }
}
