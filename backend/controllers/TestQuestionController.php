<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use common\models\search\TestQuestionSearch;
use common\models\Test;
use common\models\TestQuestion;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class TestQuestionController
 * @package backend\controllers
 */
class TestQuestionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => TestQuestion::class,
            ],
        ];
    }

    /**
     * @param int $test_id
     * @return string
     */
    public function actionIndex($test_id)
    {
        $test = $this->findTest($test_id);
        $dataProvider = new ActiveDataProvider([
            'query' => Test::findOne($test_id)->getQuestions(),
        ]);

        $searchModel = new TestQuestionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
            'test' => $test
        ]);
    }

    /**
     * @param int $id
     * @param int $test_id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id, $test_id = 0)
    {
        if ($id) {
            $model = $this->findModel($id);
        } else {
            $model = new TestQuestion();
        }

        if ($model->load(Yii::$app->request->post())) {
            if ($test_id) {
                $model->test_id = $test_id;
            }
            if ($model->save()) {
                Yii::$app->session->addFlash('success', 'Данные спешно сохранены');
                return $this->redirect(['test-question/index', 'test_id' => $test_id]);
            }
        }

        return $this->render('update', [
           'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return TestQuestion
     * @throws NotFoundHttpException
     */
    public function findModel($id)
    {
        $model = TestQuestion::findOne($id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }

    /**
     * @param integer $test_id
     * @return Test
     * @throws NotFoundHttpException
     */
    public function findTest($test_id)
    {
        $model = Test::findOne($test_id);
        if (!$model) {
            throw new NotFoundHttpException();
        }
        return $model;
    }
}
