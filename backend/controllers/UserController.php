<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use common\models\User;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class UserController
 * @package backend\controllers
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'modelClass' => User::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => User::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => User::class,
                'attribute' => 'enabled',
            ],
        ];
    }

    /**
     * @param int $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        if ($id) {
            $model = User::findOne($id);
            if (!$model) {
                throw new NotFoundHttpException;
            }
        } else {
            $model = new User;
        }

        $passwordModel = new \common\forms\UserChangePassword;

        if ($model->load(Yii::$app->request->post()) && $passwordModel->load(Yii::$app->request->post()) && $model->validate() && $passwordModel->validate()) {
            if (!empty($passwordModel->password)) {
                $model->password = $passwordModel->password;
            }

            $model->save();

            Yii::$app->session->addFlash('success', 'Пользователь сохранен');
            return $this->goBack();
        } else {
            Yii::$app->user->returnUrl = Yii::$app->request->referrer;
        }

        return $this->render('update', [
            'model' => $model,
            'passwordModel' => $passwordModel,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new \backend\forms\UserLogin;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            Yii::$app->user->login($model->user, $model->rememberMe ? 60*60*24*365 : 0);
            return $this->redirect(Yii::$app->request->referrer);
        }

        $this->layout = 'login';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
