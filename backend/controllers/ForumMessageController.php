<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use backend\controllers\actions\ToggleAction;
use backend\controllers\actions\UpdateAction;
use common\models\ForumMessage;
use common\models\search\ForumMessageSearch;
use yii\web\Controller;

/**
 * Class ForumMessageController
 * @package backend\controllers
 */
class ForumMessageController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => ForumMessageSearch::class,
            ],
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => ForumMessage::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => ForumMessage::class,
            ],
            'toggle' => [
                'class' => ToggleAction::class,
                'modelClass' => ForumMessage::class,
                'attribute' => 'is_moderated'
            ]
        ];
    }
}
