<?php

namespace backend\controllers;

use backend\controllers\actions\DeleteAction;
use backend\controllers\actions\IndexAction;
use common\models\Feedback;
use common\models\search\FeedbackSearch;
use yii\web\Controller;

/**
 * Class FeedbackController
 * @package backend\controllers
 */
class FeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'index' => [
                'class' => IndexAction::class,
                'searchModelClass' => FeedbackSearch::class,
            ],
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => Feedback::class,
            ],
        ];
    }
}
