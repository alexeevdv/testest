<?php

namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class GridViewButtonDelete
 * @package backend\widgets
 */
class GridViewButtonDelete extends Widget
{
    /**
     * @var string
     */
    public $gridSelector = '#w0';

    /**
     * @var string
     */
    public $label = 'Удалить';

    /**
     * @var array
     */
    public $action = ['delete'];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->view->registerJs("

            $('.gridview-delete-form .btn').click(function(e){
                if ($(this).attr('disabled'))
                {
                    e.preventDefault();
                    return false;
                }
            });

            $('{$this->gridSelector}').find(\"[name='selection[]'],[name=selection_all]\").change(function(){
                var ids = $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                $('.gridview-delete-form .btn').attr('disabled', ids.length == 0);

                $('.gridview-delete-form input[name=\"ids[]\"]').remove();

                $.each(ids, function(i, id){
                    var input = $('<input>').attr({'type':'hidden','name':'ids[]'}).val(id);
                    $('.gridview-delete-form').append(input);    
                });
                
            });
        
        ");

        return Html::beginForm($this->action, 'post', [
            'class' => 'gridview-delete-form',
            'style' => 'display: inline-block;',
        ]) . Html::submitButton('<i class="fa fa-trash"></i> ' . $this->label, [
            'class' => 'btn btn-danger',
            'disabled' => true,
            'data-confirm' => 'Вы уверены что хотите удалить выбранные элементы?',
        ]) . Html::endForm();
    }
}
