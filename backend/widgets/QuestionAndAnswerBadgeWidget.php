<?php

namespace backend\widgets;

use common\models\QuestionAndAnswer;
use yii\base\Widget;

/**
 * Class QuestionAndAnswerBadgeWidget
 * @package backend\widgets
 */
class QuestionAndAnswerBadgeWidget extends Widget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $questionsWithoutAnswers = QuestionAndAnswer::find()
            ->byWithoutAnswer()
            ->count();

        if (!$questionsWithoutAnswers) {
            return '';
        }
        return "<small class=\"label pull-right bg-green\">$questionsWithoutAnswers</small>";
    }
}
