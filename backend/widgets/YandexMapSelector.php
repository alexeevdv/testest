<?php

namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\web\View;

/**
 * Class YandexMapSelector
 * @package backend\widgets
 */
class YandexMapSelector extends Widget
{
    public $model;

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->view->registerJsFile('//api-maps.yandex.ru/2.1/?lang=ru_RU', [
            'position' => View::POS_END,
        ], 'yandex-maps');
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $id = $this->getId();

        // Москва
        if (!$this->model->lat) {
            $this->model->lat = '55.7509';
        }
        if (!$this->model->lon) {
            $this->model->lon = '37.6171';
        }

        echo Html::activeHiddenInput($this->model, 'lat');
        echo Html::activeHiddenInput($this->model, 'lon');

        $this->view->registerJs('

            ymaps.ready(function() {

                var marker;

                function updateCoordinates(coordinates) {
                    $("#' . Html::getInputId($this->model, 'lat') . '").val(coordinates[0]);
                    $("#' . Html::getInputId($this->model, 'lon') . '").val(coordinates[1]);
                    marker.geometry.setCoordinates(coordinates);
                }

                var map;

                var lat = ' . $this->model->lat . '
                var lon = ' . $this->model->lon . '

                map = new ymaps.Map("' . $id . '", {
                    center: [lat, lon],
                    zoom: 10,
                    behaviors: ["default", "scrollZoom"]
                }, {
                });

                var SearchControl = new ymaps.control.SearchControl({
                    options: {
                        noPlacemark: true,
                        noPopup: true,
                    }
                });
                map.controls.remove("searchControl");
                map.controls.add(SearchControl);                
                map.controls.add("zoomControl");                
                map.controls.add("typeSelector");                
                //map.controls.add("mapTools");
 
                //Отслеживаем событие выбора результата поиска
                SearchControl.events.add("resultselect", function (e) {
                    updateCoordinates(SearchControl.getResultsArray()[0].geometry.getCoordinates());
                });
 
                marker = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: [lat, lon]
                    },

                },{
                    draggable: true
                });
                marker.events.add("dragend", function(e){
                    updateCoordinates(marker.geometry.getCoordinates());
                });
                map.geoObjects.add(marker);

                map.events.add("click", function(e){
                    updateCoordinates(e.get("coords"));
                });

                map.events.fire("dragEnd");
                
                $("#' . $id . '").on("geocode", function(event, q){
                    var geocoder = ymaps.geocode(q);
                    geocoder.then(function(res){
                        var object = res.geoObjects.get(0);
                        if (object)
                        {
                            map.setCenter(object.geometry.getCoordinates());
                            updateCoordinates(marker.geometry.getCoordinates(), map.getZoom());
                        }
                    });
                });
            });

        ');

        echo Html::tag(
            'div',
            Html::tag(
                'div',
                '',
                [
                    'id' => $id,
                    'style' => 'height: 500px;',
                ]
            ),
            [
                'class' => 'form-group',
            ]
        );
    }
}
