<?php

namespace backend\widgets;

use bajadev\ckeditor\CKEditor;
use yii\helpers\Url;

/**
 * Class WysiwygWidget
 * @package backend\widgets
 *
 * Обертка над bajadev\ckeditor, которая позволяет определить все необходимые опции один раз
 * filebrowserBrowseUrl и filebrowserUploadUrl указывают на standalone actions для загрузки и просмотра файлов
 */
class WysiwygWidget extends CKEditor
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->editorOptions = [
            'preset' => 'basic', /* basic, standart, full */
            'inline' => false,
            'filebrowserBrowseUrl' => Url::to(['site/browse-images']),
            'filebrowserUploadUrl' => Url::to(['site/upload-images']),
            'extraPlugins' => 'imageuploader',
        ];

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();
    }
}
