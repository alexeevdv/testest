<?php

namespace backend\widgets;

use kartik\date\DatePicker;
use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Class DateGridViewFilter
 * @package backend\widgets
 */
class DateGridViewFilter extends InputWidget
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        return '<div class="date-grid-view-filter input-group">' .
            '<span class="input-group-addon">' .
            Html::activeDropDownList($this->model, $this->attribute . '_date_filter', [
                '<' => '<',
                '=' => '=',
                '>' => '>',
            ], ['class' => 'form-control']) .
            '</span>' .
            DateTimePicker::widget([
                'model' => $this->model,
                'attribute' => $this->attribute,
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [

                ],
            ]) .
            '</div>';
    }
}
