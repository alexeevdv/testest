<?php

namespace backend\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Class GridViewButtonCreate
 * @package backend\widgets
 */
class GridViewButtonCreate extends Widget
{
    /**
     * @var string
     */
    public $label = 'Создать';

    /**
     * @var array
     */
    public $action = ['update'];

    /**
     * @var array
     */
    public $params = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $path = array_merge($this->action, ['id' => 0], $this->params);
        return Html::a('<i class="fa fa-plus-circle"></i> ' . $this->label, Url::to($path), ['class' => 'gridview-create-btn btn btn-success']);
    }
}
