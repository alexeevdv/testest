<?php

use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use common\models\BannerCategory;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var BannerCategory $model
 */

$this->title = 'Категории баннеров';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => CheckboxColumn::class,
                ],
                [
                    'attribute' => 'name',
                    'value' => function (BannerCategory $model) {
                        return Html::a($model->name, Url::to(['banner/index', 'BannerSearch[category_id]' => $model->id]));
                    },
                    'format' => 'html',
                ],
                'position'
            ],
        ])?>
    </div>
</div>
