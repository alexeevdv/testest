<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\BannerCategory $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' категории';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Категрории',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'position') ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
