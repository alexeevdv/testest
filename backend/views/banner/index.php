<?php

use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use common\models\Banner;
use common\models\BannerCategory;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \common\models\BannerSearch $searchModel
 */

$this->title = 'Баннеры';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => CheckboxColumn::className(),
                ],
                [
                    'attribute' => 'category_id',
                    'value' => function (Banner $model) {
                        return $model->category->name;
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'category_id',
                        ArrayHelper::map(BannerCategory::find()->orderBy(['name' => SORT_ASC])->all(), 'id', 'name'),
                        [
                            'prompt' => 'Выберите категорию',
                            'class' => 'form-control',
                        ]
                    ),
                ],
                [
                    'attribute' => 'image',
                    'filter' => false,
                ],
                [
                    'attribute' => 'url',
                    'filter' => false,
                ],
            ],
        ])?>
    </div>
</div>
