<?php

use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use backend\widgets\GridViewToggleColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \common\models\TestQuestion $question
 */

$this->title = 'Варианты ответа';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget([
        'params' => ['question_id' => $question->id]
    ]),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
$this->params['breadcrumbs'] = [
    [
        'label' => 'Тесты',
        'url' => ['index'],
    ],
    $this->title
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => CheckboxColumn::className(),
                ],
                'text:html',
                [
                    'class' => GridViewToggleColumn::className(),
                    'attribute' => 'is_right'
                ]
            ],
        ])?>
    </div>
</div>
