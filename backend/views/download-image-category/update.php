<?php

use common\models\DownloadImageCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var DownloadImageCategory $model
 * @var array $categoriesToShow
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' категории';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Категории',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);

?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form
            ->field($model, 'parent_id')
            ->dropDownList($categoriesToShow, ['prompt' => '/'])
        ?>

        <?= $form->field($model, 'name') ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
