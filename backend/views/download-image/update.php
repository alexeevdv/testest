<?php

use alexeevdv\image\SingleImageUploadWidget;
use common\models\DownloadImage;
use common\models\DownloadImageCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var DownloadImage $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' изображения';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Изображения',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form->field($model, 'name') ?>

        <?= $form
            ->field($model, 'category_id')
            ->dropDownList(ArrayHelper::map(DownloadImageCategory::find()->all(), 'id', 'name'))
        ?>

        <?= $form
            ->field($model, 'file')
            ->widget(SingleImageUploadWidget::class)
        ?>

        <?= $form->field($model, 'preview') ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
