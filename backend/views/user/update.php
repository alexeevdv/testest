<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' пользователя';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Пользователи',
        'url' => ['index'],
    ],
    $this->title
];

?>
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false
    ]);

    echo $form->field($model, 'name');

    echo $form->field($model, 'email');

    echo $form->field($model, 'role')
              ->dropDownList([
                  'admin' => 'admin',
                  'user' => 'user',
              ]);

    echo $form->field($passwordModel, 'password')->passwordInput();

    echo $form->field($passwordModel, 'password_confirmation')->passwordInput();

?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
