<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->registerJs("

    jQuery('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

");

?>
<div class="user_login">
    <div class="login-box">
        <div class="box-header login-logo">
            Панель управления
        </div>
          <div class="login-box-body">
            <p class="login-box-msg"></p>
            <?php
                $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                ]);

                echo $form->field($model, 'email')
                          ->input('email', ['placeholder' => 'Email'])
                          ->label(false);

                echo $form->field($model, 'password')
                          ->passwordInput(['placeholder' => 'Пароль'])
                          ->label(false);
            ?>

            <div class="row">
                <div class="col-xs-8">
                   <div class="checkbox icheck">
                    <?php
                        echo $form->field($model, 'rememberMe')
                                  ->checkbox(['label' => 'Запомнить меня']);
                    ?>
                    </div>
                </div>
                <div class="col-xs-4">
                    <?=Html::submitButton('Войти', [ 'class' => 'btn btn-primary btn-block btn-flat'])?>
                </div>
            </div>
        <?php
            $form->end();
        ?>
        </div>
        <!-- /.login-box-body -->
        <div class="box-footer">
            <!--
            <a href="http://ejen.ru"><?=Html::img(['/images/ejen-logo.png'])?></a>
            -->
        </div>
    </div>
    <!-- /.login-box -->
</div>
