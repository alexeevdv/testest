<?php

use backend\widgets\QuestionAndAnswerBadgeWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

\backend\assets\AppAsset::register($this);

$this->title = $this->title . ' | Панель Управления';

?>
<?php $this->beginPage()?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="<?= Yii::$app->charset ?>">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
<?php $this->beginBody(); ?>
<div class="wrapper">
  <header class="main-header">

    <!-- Logo -->
    <a data-toggle="offcanvas" role="button" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><i class="fa fa-bars"></i></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <?=\common\widgets\UserImage::widget([
                    'model' => Yii::$app->user->identity,
                    'htmlOptions' => [
                        'class' => 'user-image',
                    ],
                ])?>
              <span class="hidden-xs"><?=Yii::$app->user->identity->name?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                    <?=\common\widgets\UserImage::widget([
                        'model' => Yii::$app->user->identity,
                        'htmlOptions' => [
                            'class' => 'img-circle',
                        ],
                    ])?>
                    <p>
                    <?php
                        echo Yii::$app->user->identity->name;
                    ?>
                    </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                    <a href="<?=Url::to(['/user/update', 'id' => Yii::$app->user->id])?>" class="btn btn-default btn-flat">Профиль</a>
                </div>
                <div class="pull-right">
                    <a href="<?=Url::to(['/user/logout'])?>" class="btn btn-default btn-flat">Выйти</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <?=Menu::widget([
            'encodeLabels' => false,
            'options' => [
                'class' => 'sidebar-menu',
            ],
            'activateParents' => true,
            'submenuTemplate' => "\n<ul class='treeview-menu'>\n{items}\n</ul>\n",
            'items' => [
                [
                    'label' => '<i class="fa fa-file-text"></i> <span>Cтраницы</span>',
                    'url' => ['/page/index'],
                ],
                [
                    'label' => '<i class="fa fa-user"></i> <span>Пользователи</span>',
                    'url' => ['/user/index'],
                ],
//                [
//                    'label' => '<i class="fa fa-list"></i> <span>Новости</span>',
//                    'url' => ['/news/index'],
//                ],
                [
                    'label' => '<i class="fa fa-chevron-right"></i> <span>Статьи</span>',
                    'url' => '#',
                    'options' => [
                        'class' => 'treeview'
                    ],
                    'items' => [
                        [
                            'label' => 'Статьи',
                            'url' => ['/article/index'],
                            'active' => Yii::$app->controller->id == 'article',
                        ],
                        [
                            'label' => 'Категории статей',
                            'url' => ['/article-category/index'],
                            'active' => Yii::$app->controller->id == 'article-category',
                        ],
                    ]
                ],
                [
                    'label' => '<i class="fa fa-list"></i> <span>Опросы</span>',
                    'url' => ['/polling/index'],
                ],
                [
                    'label' => '<i class="fa fa-list"></i> <span>Документы</span>',
                    'url' => ['/document/index'],
                ],
                [
                    'label' => '<i class="fa fa-chevron-right"></i> <span>Баннеры</span>',
                    'url' => '#',
                    'options' => [
                        'class' => 'treeview'
                    ],
                    'items' => [
                        [
                            'label' => 'Баннеры',
                            'url' => ['/banner/index'],
                            'active' => Yii::$app->controller->id == 'banner',
                        ],
                        [
                            'label' => 'Категории баннеров',
                            'url' => ['/banner-category/index'],
                            'active' => Yii::$app->controller->id == 'banner-category',
                        ],
                    ]
                ],
                [
                    'label' => '<i class="fa fa-chevron-right"></i> <span>Загрузки</span>',
                    'url' => '#',
                    'options' => [
                        'class' => 'treeview'
                    ],
                    'items' => [
                        [
                            'label' => 'Изображения',
                            'url' => ['/download-image/index'],
                            'active' => Yii::$app->controller->id == 'download-image',
                        ],
                        [
                            'label' => '&nbsp;&nbsp;-&nbsp;&nbsp;Категории изображений',
                            'url' => ['/download-image-category/index'],
                            'active' => Yii::$app->controller->id == 'download-image-category',
                        ],
                        [
                            'label' => 'Видео',
                            'url' => ['/download-video/index'],
                            'active' => Yii::$app->controller->id == 'download-video',
                        ],
                    ]
                ],
                [
                    'label' => '<i class="fa fa-question-circle"></i>
                                <span>Вопрос - Ответ</span>
                                <span class="pull-right-container">
                                       ' . QuestionAndAnswerBadgeWidget::widget() . '
                                </span>',
                    'url' => ['/question-and-answer/index'],
                ],
                [
                    'label' => '<i class="fa fa-chevron-right"></i> <span>Обратная связь</span>',
                    'url' => '#',
                    'options' => [
                        'class' => 'treeview'
                    ],
                    'items' => [
                        [
                            'label' => 'Вопросы',
                            'url' => ['/feedback/index'],
                            'active' => Yii::$app->controller->id == 'feedback',
                        ],
                        [
                            'label' => 'Категории вопросов',
                            'url' => ['/feedback-category/index'],
                            'active' => Yii::$app->controller->id == 'feedback-category',
                        ],
                    ]
                ],
                [
                    'label' => '<i class="fa fa-chevron-right"></i> <span>Организации</span>',
                    'url' => '#',
                    'options' => [
                        'class' => 'treeview'
                    ],
                    'items' => [
                        [
                            'label' => 'Организации',
                            'url' => ['/organization/index'],
                            'active' => Yii::$app->controller->id == 'organization',
                        ],
                        [
                            'label' => 'Типы организаций',
                            'url' => ['/organization-type/index'],
                            'active' => Yii::$app->controller->id == 'organization-type',
                        ],
                        [
                            'label' => 'Регионы',
                            'url' => ['/region/index'],
                            'active' => Yii::$app->controller->id == 'region',
                        ],
                        [
                            'label' => 'Города',
                            'url' => ['/city/index'],
                            'active' => Yii::$app->controller->id == 'city',
                        ],
                    ]
                ],
                [
                    'label' => '<i class="fa fa-check-circle-o"></i> <span>Тесты</span>',
                    'url' => ['/test/index'],
                ],
                [
                    'label' => '<i class="fa fa-chevron-right"></i> <span>Форум</span>',
                    'url' => '#',
                    'options' => [
                        'class' => 'treeview'
                    ],
                    'items' => [
                        [
                            'label' => 'Сообщения',
                            'url' => ['/forum-message/index'],
                            'active' => Yii::$app->controller->id == 'forum-message',
                        ],
                        [
                            'label' => 'Разделы',
                            'url' => ['/forum-section/index'],
                            'active' => Yii::$app->controller->id == 'forum-section',
                        ],
                        [
                            'label' => 'Темы',
                            'url' => ['/forum-theme/index'],
                            'active' => Yii::$app->controller->id == 'forum-theme',
                        ],
                    ]
                ],
            ],
        ])?>
    </section>
    <!-- /.sidebar -->
  </aside>
<div class="content-wrapper">
 <!-- Content Header (Page header) -->
    <section class="content-header">
        <?php if (isset($this->params['header'])) : ?>
            <h1><?=$this->params['header']?></h1>
        <?php endif; ?>
        <?php if (isset($this->params['breadcrumbs'])) : ?>
        <?=Breadcrumbs::widget([
            'options' => [
                'class' => 'breadcrumb',
            ],
            'encodeLabels' => false,
            'homeLink' => [
                'label' => '<i class="fa fa-dashboard"></i> Главная',
                'url' => ['/site/index'],
            ],
            'links' => $this->params['breadcrumbs'],
        ])?>
        <?php endif; ?>
    </section>

        <?php
        if (isset($this->params['toolbar'])) {
            echo '<div class="content-header">';
            foreach ($this->params['toolbar'] as $button) {
                echo $button . '&nbsp;&nbsp;';
            }
            echo '</div>';
        }
        ?>
    <!-- Main content -->
    <section class="content">
    <?php
    if (count(Yii::$app->session->getAllFlashes())) {
        foreach (Yii::$app->session->getAllFlashes() as $key => $messages) {
            foreach ($messages as $message) {
                echo '<div class="callout callout-' . $key . '"><p>' . \yii::t('app', $message) . '</p></div>';
            }
        }
    }
    ?>
        <?= $content ?>
    </section>
</div>
</div>
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
