<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' опроса';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Опросы',
        'url' => ['index'],
    ],
    $this->title
];

?>
<div class="box box-primary">
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]);

        echo $form->field($model, 'is_active')->widget(BootstrapToggleWidget::class);

        echo $form->field($model, 'name');

        ?>
    </div>
    <div class="box-footer">
        <?php
        echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);
        $form->end();
        ?>
    </div>
</div>
