<?php

use backend\widgets\DateGridViewFilter;
use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use backend\widgets\GridViewToggleColumn;
use common\models\Article;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var Article $model
 */

$this->title = 'Статьи';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => CheckboxColumn::class,
                ],
                'title:text',
                'slug:text',
                [
                    'label' => 'Категория',
                    'attribute' => 'categoryTitle',
                    'value' => 'category.title'
                ],
                [
                    'attribute' => 'published_at',
                    'filter' => DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'published_at']),
                ],
                [
                    'class' => GridViewToggleColumn::class,
                    'attribute' => 'is_enabled'
                ]
            ],
        ])?>
    </div>
</div>
