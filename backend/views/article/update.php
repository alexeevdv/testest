<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use alexeevdv\image\SingleImageUploadWidget;
use alexeevdv\widget\SluggableInputWidget;
use backend\widgets\WysiwygWidget;
use common\models\ArticleCategory;
use common\widgets\DateTimePickerWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\Article $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' статьи';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Статьи',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form
            ->field($model, 'is_enabled')
            ->widget(BootstrapToggleWidget::className());
        ?>

        <?= $form->field($model, 'published_at')->widget(DateTimePickerWidget::className()) ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'slug')->widget(SluggableInputWidget::className(), [
            'dependsOn' => 'title'
        ]) ?>

        <?= $form->field($model, 'category_id')
            ->dropDownList(ArrayHelper::map(ArticleCategory::find()->all(), 'id', 'title')) ?>

        <?= $form->field($model, 'image')->widget(SingleImageUploadWidget::className()) ?>

        <?= $form->field($model, 'short_text')->widget(WysiwygWidget::className()) ?>

        <?= $form->field($model, 'text')->widget(WysiwygWidget::className()) ?>

        <?= $form->field($model, 'seo_title') ?>

        <?= $form->field($model, 'seo_keywords') ?>

        <?= $form->field($model, 'seo_description')->textarea() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
