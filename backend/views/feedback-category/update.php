<?php

use common\models\FeedbackCategory;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var FeedbackCategory $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' категории';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Обратная связь',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form->field($model, 'name') ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
