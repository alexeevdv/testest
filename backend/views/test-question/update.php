<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\Article $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' Вопроса';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Тесты',
        'url' => ['test/index'],
    ],
    [
        'label' => 'вопросы',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form->field($model, 'is_checkbox')->dropDownList(['0' => 'Radiobutton', '1' => 'checkbox'])?>

        <?= $form->field($model, 'text')->textarea() ?>

        <?= $form->field($model, 'comment')->textarea() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
