<?php

use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use common\models\Test;
use common\models\TestQuestion;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var ActiveDataProvider $dataProvider
 * @var Test $test
 */

$this->title = 'Вопросы теста';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget([
        'params' => ['test_id' => $test->id]
    ]),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
$this->params['breadcrumbs'] = [
    [
        'label' => 'Тесты',
        'url' => ['index'],
    ],
    $this->title
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => CheckboxColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                ],
                'text:text',
                [
                    'attribute' => 'is_checkbox',
                    'value' => function (TestQuestion $model) {
                        return ($model->is_checkbox ? 'checkbox' : 'radiobutton');
                    },
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'is_checkbox',
                        [
                            '0' => 'raddiobutton',
                            '1' => 'checkbox',
                        ],
                        [
                            'class' => 'form-control',
                            'prompt' => ''
                        ]
                    ),
                ],
                'comment',
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => ActionColumn::class,
                    'header' => 'Добавить вариант ответа',
                    'template' => '{update}',
                    'urlCreator' => function ($action, TestQuestion $model, $key, $index) {
                        if ($action === 'update') {
                            $url = Url::to(['test-answer/index', 'question_id' => $model->id]);
                            return $url;
                        }
                    }
                ],
            ],
        ])?>
    </div>
</div>
