<?php

use common\models\Region;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\City $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' города';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Организации',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form
            ->field($model, 'region_id')
            ->dropDownList(ArrayHelper::map(Region::find()->all(), 'id', 'name'))
        ?>

        <?= $form->field($model, 'name') ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
