<?php

use backend\widgets\DateGridViewFilter;
use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use backend\widgets\GridViewToggleColumn;
use yii\data\ActiveDataProvider;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Сообщения';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => CheckboxColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                ],
                'text:html',
                [
                    'attribute' => 'created_at',
                    'filter'=> DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'created_at']),
                ],
                [
                    'label' => 'Тема',
                    'attribute' => 'themeTitle',
                    'value' => 'theme.title'
                ],
                [
                    'label' => 'Автор',
                    'attribute' => 'authorName',
                    'value' => 'author.name'
                ],
                [
                    'class' => GridViewToggleColumn::class,
                    'attribute' => 'is_moderated'
                ]
            ],
        ])?>
    </div>
</div>
