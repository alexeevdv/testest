<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use backend\widgets\WysiwygWidget;
use common\models\ForumMessage;
use common\models\ForumTheme;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var ForumMessage $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' сообщения';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Сообщения',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form
            ->field($model, 'is_moderated')
            ->widget(BootstrapToggleWidget::class);
        ?>

        <?= $form
            ->field($model, 'theme_id')
            ->dropDownList(ArrayHelper::map(ForumTheme::find()->all(), 'id', 'title')) ?>

        <?= $form
            ->field($model, 'text')
            ->widget(WysiwygWidget::class)
        ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
