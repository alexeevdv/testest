<?php

use common\models\ForumSection;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var ForumSection $model
 * @var ForumSection[] $parents
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' раздела';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Разделы',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form
            ->field($model, 'parent_id')
            ->dropDownList(
                ArrayHelper::map($parents, 'id', 'title'),
                ['prompt' => '...']
            )
        ?>

        <?= $form->field($model, 'title') ?>

        <?= $form
            ->field($model, 'description')
            ->textarea()
        ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
