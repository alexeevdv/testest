<?php

use backend\widgets\DateGridViewFilter;
use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use yii\data\ActiveDataProvider;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Разделы';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => CheckboxColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                ],
                [
                    'label' => 'Родительский раздел',
                    'attribute' => 'parent_title',
                    'value' => 'parent.title'
                ],
                'title:text',
                [
                    'attribute' => 'created_at',
                    'filter'=> DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'created_at']),
                ],
            ],
        ])?>
    </div>
</div>
