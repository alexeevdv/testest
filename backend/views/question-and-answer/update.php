<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use backend\widgets\WysiwygWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\QuestionAndAnswer $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' ответа';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Вопрос-ответ',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form->field($model, 'is_enabled')->widget(BootstrapToggleWidget::class) ?>

        <?= $form->field($model, 'question')->widget(WysiwygWidget::class) ?>

        <?= $form->field($model, 'answer')->widget(WysiwygWidget::class) ?>

        <?= $form->field($model, 'answer_user_id')->label(false)->hiddenInput(['value' => Yii::$app->user->getId()]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
