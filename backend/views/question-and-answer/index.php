<?php

use backend\widgets\DateGridViewFilter;
use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use backend\widgets\GridViewToggleColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Вопрос-ответ';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => CheckboxColumn::class,
                ],
                'question:html',
                [
                    'attribute' => 'created_at',
                    'filter' => DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'created_at']),
                ],
                'answer:html',
                [
                    'class' => GridViewToggleColumn::class,
                    'attribute' => 'is_enabled'
                ]
            ],
        ])?>
    </div>
</div>
