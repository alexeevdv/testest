<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use alexeevdv\widget\SluggableInputWidget;
use backend\widgets\WysiwygWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' страницы';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Страницы',
        'url' => ['index'],
    ],
    $this->title
];

?>
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'published')->widget(BootstrapToggleWidget::class);

    echo $form->field($model, 'name');

    echo $form->field($model, 'slug')->widget(SluggableInputWidget::class, [
        'dependsOn' => 'name'
    ]);

    echo $form->field($model, 'text')->widget(WysiwygWidget::class);

    echo $form->field($model, 'seo_title');

    echo $form->field($model, 'seo_keywords');

    echo $form->field($model, 'seo_description')->textarea();

?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
