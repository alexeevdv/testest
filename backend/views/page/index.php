<?php

use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewToggleColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Страницы';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    Html::a('<i class="fa fa-plus-circle"></i> Создать', ['update', 'id' => 0], ['class' => 'btn btn-success']),
    GridViewButtonDelete::widget()
];
?>
<div class="box box-primary">
    <div class="box-body">
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => CheckboxColumn::class,
                'headerOptions' => [
                    'width' => 50,
                ],
            ],
            'name',
            'slug',
            [
                'class' => GridViewToggleColumn::class,
                'attribute' => 'published',
            ]
        ],
    ])?>
    </div>
</div>
