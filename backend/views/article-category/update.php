<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use alexeevdv\widget\SluggableInputWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\ArticleCategory $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' категории';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Категории',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form->field($model, 'is_enabled')->widget(BootstrapToggleWidget::className()) ?>

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'slug')->widget(SluggableInputWidget::className(), [
            'dependsOn' => 'title'
        ]) ?>

        <?= $form->field($model, 'seo_title') ?>

        <?= $form->field($model, 'seo_keywords') ?>

        <?= $form->field($model, 'seo_description')->textarea() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
