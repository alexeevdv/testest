<?php

use backend\widgets\DateGridViewFilter;
use backend\widgets\GridViewButtonDelete;
use common\models\FeedbackCategory;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Обратная связь';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => CheckboxColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                ],
                'name:text',
                'email:text',
                'text',
                [
                    'label' => 'Категория',
                    'attribute' => 'category_id',
                    'value' => 'category.name',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'category_id',
                        ArrayHelper::map(FeedbackCategory::find()->all(), 'id', 'name'),
                        [
                            'class' => 'form-control',
                            'prompt' => ''
                        ]
                    )
                ],
                [
                    'attribute' => 'created_at',
                    'filter' => DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'created_at']),
                ],
            ],
        ])?>
    </div>
</div>
