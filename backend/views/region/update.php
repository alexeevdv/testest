<?php

use common\models\Region;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var Region $model
 */

$this->title = $model->isNewRecord ? 'Добавление региона' : 'Редактирование региона';
$this->params['header'] = $this->title;
$this->params['breacrumbs'] = [
    [
        'label' => 'Регионы',
        'url' => ['index'],
    ],
    $this->title,
];

?>
<div class="organization-type_update">
<?php
$form = ActiveForm::begin();
?>
<div class="box">
    <div class="box-body">
    <?php
        echo $form->field($model, 'name');
    ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
