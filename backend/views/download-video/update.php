<?php

use alexeevdv\image\SingleImageUploadWidget;
use alexeevdv\file\SingleFileUploadWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' видео';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Видео',
        'url' => ['index'],
    ],
    $this->title
];

?>
<div class="box box-primary">
    <div class="box-body">
<?php
    $form = ActiveForm::begin([
        'enableClientValidation' => false,
    ]);

    echo $form->field($model, 'title');

    echo $form
        ->field($model, 'preview')
        ->widget(SingleImageUploadWidget::class);

    echo $form
        ->field($model, 'file')
        ->widget(SingleFileUploadWidget::class);

?>
    </div>
    <div class="box-footer">
<?php
    echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);
    $form->end();
?>
    </div>
</div>
