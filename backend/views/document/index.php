<?php

use backend\widgets\DateGridViewFilter;
use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use common\models\Document;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var Document $model
 */

$this->title = 'Документы';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => CheckboxColumn::className(),
                ],
                'title:text',
                'author:text',
                [
                    'attribute' => 'start_date',
                    'filter' => DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'start_date']),
                ],
                [
                    'attribute' => 'end_date',
                    'filter' => DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'end_date']),
                ],
            ],
        ])?>
    </div>
</div>
