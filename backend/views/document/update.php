<?php

use alexeevdv\file\SingleFileUploadWidget;
use common\widgets\DatePickerWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\Document $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' документа';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Документы',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form->field($model, 'title') ?>

        <?= $form->field($model, 'author') ?>

        <?= $form->field($model, 'number') ?>

        <?= $form
            ->field($model, 'start_date')
            ->widget(DatePickerWidget::class)
        ?>

        <?= $form
            ->field($model, 'end_date')
            ->widget(DatePickerWidget::class)
        ?>

        <?= $form
            ->field($model, 'file')
            ->widget(SingleFileUploadWidget::class)
        ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
