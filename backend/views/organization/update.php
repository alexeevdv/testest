<?php

use backend\widgets\YandexMapSelector;
use common\models\Organization;
use common\models\OrganizationType;
use common\models\Region;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var Organization $model
 * @var OrganizationType[] $types
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' организации';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Организации',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= Html::tag(
            'label',
            'Регион',
            [
                'class' => 'control-label'
            ]
        )?>
        <?= Html::dropDownList(
            'Region',
            false,
            ArrayHelper::map(Region::find()->all(), 'id', 'name'),
            ['id' => 'region-id', 'prompt' => '...', 'class' => 'form-control']
        )?>

        <?= $form
            ->field($model, 'city_id')
            ->widget(DepDrop::class, [
                'data' => $model->isNewRecord ? [0 => '...'] : ([$model->city_id => $model->city->name]),
                'pluginOptions' => [
                    'depends' => ['region-id'],
                    'placeholder' => '...',
                    'url' => Url::to(['city/depends-on-region-list'])
                ]
            ])
        ?>

        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'email') ?>

        <?= $form->field($model, 'site') ?>

        <?= $form->field($model, 'phones') ?>

        <?= $form->field($model, 'address') ?>

        <?= $form
            ->field($model, 'types')
            ->widget(Select2::class, [
                'data' => ArrayHelper::map($types, 'id', 'name'),
                'options' => [
                    'multiple' => true,
                    'placeholder' => 'Выберите типы',
                ],
            ])
        ?>

        <?= YandexMapSelector::widget([
            'model' => $model,
        ])?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
