<?php

use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use common\models\Organization;
use common\models\OrganizationType;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Организации';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => CheckboxColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                ],
                'name:text',
                'email:text',
                [
                    'label' => 'Город',
                    'attribute' => 'cityName',
                    'value' => 'city.name'
                ],
                [
                    'label' => 'Тип организации',
                    'attribute' => 'typeId',
                    'value' => function (Organization $model) {
                        if (!count($model->types)) {
                            return null;
                        }
                        return implode('<br>', ArrayHelper::getColumn($model->types, 'name'));
                    },
                    'format' => 'html',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'typeId',
                        ArrayHelper::map(OrganizationType::find()->all(), 'id', 'name'),
                        [
                            'class' => 'form-control',
                            'prompt' => ''
                        ]
                    ),
                ]
            ],
        ])?>
    </div>
</div>
