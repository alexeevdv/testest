<?php

use common\models\OrganizationType;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var OrganizationType $model
 */

$this->title = $model->isNewRecord ? 'Добавление типа' : 'Редактирование типа';
$this->params['header'] = $this->title;
$this->params['breacrumbs'] = [
    [
        'label' => 'Типы организаций',
        'url' => ['index'],
    ],
    $this->title,
];

?>
<div class="organization-type_update">
<?php
$form = ActiveForm::begin();
?>
<div class="box">
    <div class="box-body">
    <?php
        echo $form->field($model, 'name');
    ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
