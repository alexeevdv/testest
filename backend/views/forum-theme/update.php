<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use common\models\ForumSection;
use common\models\ForumTheme;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var ForumTheme $model
 * @var ForumSection[] $sections
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' темы';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Темы',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form
            ->field($model, 'is_closed')
            ->widget(BootstrapToggleWidget::class);
        ?>

        <?= $form
            ->field($model, 'is_pinned')
            ->widget(BootstrapToggleWidget::class);
        ?>

        <?= $form
            ->field($model, 'section_id')
            ->dropDownList(ArrayHelper::map($sections, 'id', 'title'))
        ?>

        <?= $form->field($model, 'title') ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
