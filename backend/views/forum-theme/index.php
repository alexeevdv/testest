<?php

use backend\widgets\DateGridViewFilter;
use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use backend\widgets\GridViewToggleColumn;
use common\models\ForumTheme;
use yii\data\ActiveDataProvider;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var ActiveDataProvider $dataProvider
 */

$this->title = 'Темы';

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [$this->title];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => CheckboxColumn::class,
                    'headerOptions' => [
                        'width' => 50,
                    ],
                ],
                'title:text',
                [
                    'label' => 'Автор',
                    'attribute' => 'authorName',
                    'value' => 'author.name'
                ],
                [
                    'label' => 'Подраздел',
                    'attribute' => 'sectionTitle',
                    'value' => 'section.title'
                ],
                [
                    'attribute' => 'created_at',
                    'filter'=> DateGridViewFilter::widget(['model' => $searchModel, 'attribute' => 'created_at']),
                ],
                [
                    'class' => GridViewToggleColumn::class,
                    'attribute' => 'is_closed'
                ],
                [
                    'class' => GridViewToggleColumn::class,
                    'attribute' => 'is_pinned',
                    'url' => function (ForumTheme $model) {
                        return ['forum-theme/toggle', 'id' => $model->id];
                    }
                ]
            ],
        ])?>
    </div>
</div>
