<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use backend\widgets\WysiwygWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' варианта';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Страницы',
        'url' => ['index'],
    ],
    $this->title
];

?>
<div class="box box-primary">
    <div class="box-body">
        <?php
        $form = ActiveForm::begin([
            'enableClientValidation' => false,
        ]);

        echo $form
            ->field($model, 'has_user_variant')
            ->widget(BootstrapToggleWidget::class);

        echo $form
            ->field($model, 'text')
            ->widget(WysiwygWidget::class);

        ?>
    </div>
    <div class="box-footer">
        <?php
        echo Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']);
        $form->end();
        ?>
    </div>
</div>
