<?php

use backend\widgets\GridViewButtonCreate;
use backend\widgets\GridViewButtonDelete;
use backend\widgets\GridViewButtonUpdate;
use backend\widgets\GridViewToggleColumn;
use yii\grid\CheckboxColumn;
use yii\grid\GridView;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 */

$this->title = 'Варианты ответов к опросу ' . $polling->name;

$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Опросы',
        'url' => ['polling/index'],
    ],
    $this->title
];
$this->params['toolbar'] = [
    GridViewButtonCreate::widget(['params' => ['polling_id' => $polling->id]]),
    GridViewButtonUpdate::widget(),
    GridViewButtonDelete::widget(),
];
?>
<div class="box box-primary">
    <div class="box-body">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'headerOptions' => [
                        'width' => 50,
                    ],
                    'class' => CheckboxColumn::class,
                ],
                'text:html',
                [
                    'class' => GridViewToggleColumn::class,
                    'attribute' => 'has_user_variant',
                ]
            ],
        ])?>
    </div>
</div>
