<?php

use alexeevdv\bootstrap\BootstrapToggleWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var \common\models\Article $model
 */

$this->title = ($model->isNewRecord ? 'Добавление' : 'Редактирование') . ' теста';
$this->params['header'] = $this->title;
$this->params['breadcrumbs'] = [
    [
        'label' => 'Тесты',
        'url' => ['index'],
    ],
    $this->title
];

$form = ActiveForm::begin([
    'enableClientValidation' => false,
]);
?>
<div class="box box-primary">
    <div class="box-body">

        <?= $form
            ->field($model, 'is_enabled')
            ->widget(BootstrapToggleWidget::className());
        ?>

        <?= $form->field($model, 'title')->textarea() ?>


    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-save"></i> Сохранить', ['class' => 'btn btn-success']) ?>
    </div>
</div>
<?php
$form->end();
?>
