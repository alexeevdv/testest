Установка
=========

Устанавливаем зависимости проекта
```
composer install
```

Инициализируем проект
```
./init
```

Настраиваем подключение к базе данных в /common/config/main-local.php (если развернуто через Docker, то этот пункт можно пропустить)

```
./yii migrate
```

Данные для входа
================

Email: ejen@ejen.ru
Пароль: ejen
