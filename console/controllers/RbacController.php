<?php

namespace console\controllers;

use Yii;

use common\rbac\UserRoleRule;
use yii\console\Controller;

/**
 * Class RbacController
 * @package console\controllers
 */
class RbacController extends Controller
{
    /**
     *
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();

        $rule_userRole = new UserRoleRule;
        $auth->add($rule_userRole);

        // user
        $role_user = $auth->createRole('user');
        $role_user->ruleName = $rule_userRole->name;
        $auth->add($role_user);

        // admin
        $role_admin = $auth->createRole('admin');
        $role_admin->ruleName = $rule_userRole->name;
        $auth->add($role_admin);

        //
        $auth->addChild($role_admin, $role_user);
    }
}
