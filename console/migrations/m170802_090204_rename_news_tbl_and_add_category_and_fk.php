<?php

use yii\db\Migration;

/**
 * Class m170802_090204_rename_news_tbl_and_add_category_and_fk
 */
class m170802_090204_rename_news_tbl_and_add_category_and_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameTable('news', 'article');

        $this->addColumn('article', 'category_id', $this->integer()->after('id'));
        $this->addColumn('article', 'image', $this->string()->after('title'));
        $this->addColumn('article', 'short_text', $this->text()->after('text'));
        $this->addColumn('article', 'is_enabled', $this->boolean()->defaultValue(false));

        $this->createTable('article_category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'slug' => $this->string(),
            'is_enabled' => $this->boolean()->defaultValue(false),
            'seo_title' => $this->string(),
            'seo_description' => $this->text(),
            'seo_keywords' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-article-category_id-article_category-id',
            'article',
            'category_id',
            'article_category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('article', 'fk-article-category_id-article_category-id');

        $this->dropTable('article_category');

        $this->dropColumn('article', 'is_enabled');
        $this->dropColumn('article', 'short_text');
        $this->dropColumn('article', 'image');
        $this->dropColumn('article', 'category_id');

        $this->renameTable('article', 'news');
    }
}
