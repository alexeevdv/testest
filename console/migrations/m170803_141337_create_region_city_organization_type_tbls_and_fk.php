<?php

use yii\db\Migration;

/**
 * Class m170803_141337_create_region_city_organization_type_tbls_and_fk
 */
class m170803_141337_create_region_city_organization_type_tbls_and_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);

        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'region_id' => $this->integer(),
            'name' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-city-region_id-region-id',
            'city',
            'region_id',
            'region',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('organization', [
            'id' => $this->primaryKey(),
            'city_id' => $this->integer(),
            'name' => $this->string(),
            'email' => $this->string(),
            'site' => $this->string(),
            'phones' => $this->string(),
            'address' => $this->string(),
            'lat' => $this->float(),
            'lon' => $this->float(),
        ]);

        $this->addForeignKey(
            'fk-organization-city_id-city-id',
            'organization',
            'city_id',
            'city',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-organization-city_id-city-id', 'organization');

        $this->dropTable('organization');

        $this->dropForeignKey('fk-city-region_id-region-id', 'city');

        $this->dropTable('city');
        $this->dropTable('region');
    }
}
