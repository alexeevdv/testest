<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m170803_062228_download_video
 */
class m170803_062228_download_video extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('download_video', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'preview' => $this->string(),
            'file' => $this->string(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('download_video');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_062228_download_video cannot be reverted.\n";

        return false;
    }
    */
}
