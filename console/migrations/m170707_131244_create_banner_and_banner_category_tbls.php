<?php

use yii\db\Migration;

/**
 * Class m170707_131244_create_banner_and_banner_category_tbls
 */
class m170707_131244_create_banner_and_banner_category_tbls extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('banner_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'position' => $this->string()
        ]);

        $this->createTable('banner', [
            'id' => $this->primaryKey(),
            'category_id' => $this-> integer(),
            'image' => $this->string(),
            'url' => $this->string(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('NOW()'),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->addForeignKey(
            'fk-banner-category_id',
            'banner',
            'category_id',
            'banner_category',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-banner-category_id', 'banner');

        $this->dropTable('banner');
        $this->dropTable('banner_category');
    }
}
