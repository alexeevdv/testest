<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m170803_123043_document
 */
class m170803_123043_document extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('document', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'author' => $this->string(),
            'number' => $this->string(),
            'file' => $this->string(),
            'start_date' => $this->timestamp()->defaultValue(null),
            'end_date' => $this->timestamp()->defaultValue(null),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('document');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170803_123043_document cannot be reverted.\n";

        return false;
    }
    */
}
