<?php

use yii\db\Migration;

/**
 * Class m170802_142344_create_download_image_tbl_and_category_and_fk
 */
class m170802_142344_create_download_image_tbl_and_category_and_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('download_image_category', [
            'id' => $this->primaryKey(),
            'tree' => $this->integer()->notNull(),
            'lft' => $this->integer()->notNull(),
            'rgt' => $this->integer()->notNull(),
            'depth' => $this->integer()->notNull(),
            'name' => $this->string()->notNull()
        ]);

        $this->createTable('download_image', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'name' => $this->string(),
            'preview' => $this->string(),
            'file' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-download_image-category_id-download_image_category-id',
            'download_image',
            'category_id',
            'download_image_category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-download_image-category_id-download_image_category-id', 'download_image');

        $this->dropTable('download_image');
        $this->dropTable('download_image_category');
    }
}
