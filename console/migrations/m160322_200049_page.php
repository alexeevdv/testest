<?php

use yii\db\Migration;

/**
 * Class m160322_200049_page
 */
class m160322_200049_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('page', [
            'id'   => $this->primaryKey(),
            'name' => $this->string(),
            'slug' => $this->string(),
            'text' => $this->text(),
            'seo_title' => $this->string(),
            'seo_keywords' => $this->string(),
            'seo_description' => $this->string(),
            'published' => $this->boolean()->defaultValue(true),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('NOW()'),
            'updated_at' => $this->timestamp()->null(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('page');
    }
}
