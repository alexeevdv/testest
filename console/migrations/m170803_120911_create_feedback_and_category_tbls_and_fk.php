<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m170803_120911_create_feedback_and_category_tbls_and_fk
 */
class m170803_120911_create_feedback_and_category_tbls_and_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('feedback', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer(),
            'user_id' => $this->integer(),
            'name' => $this->string(),
            'email' => $this->string(),
            'text' => $this->text(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null()
        ]);

        $this->createTable('feedback_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $this->addForeignKey(
            'fk-feedback-category_id-feedback_category-id',
            'feedback',
            'category_id',
            'feedback_category',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-feedback-category_id-feedback_category-id', 'feedback');

        $this->dropTable('feedback_category');

        $this->dropTable('feedback');
    }
}
