<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m170802_122511_create_question_and_answer_tbl_and_fk
 */
class m170802_122511_create_question_and_answer_tbl_and_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('question_and_answer', [
            'id' => $this->primaryKey(),
            'question_user_id' => $this->integer(),
            'question' => $this->text(),
            'answer_user_id' => $this->integer(),
            'answer' => $this->text(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null(),
            'is_enabled' => $this->boolean()->defaultValue(false)
        ]);

        $this->addForeignKey(
            'fk-question_and_answer-question_user_id-user-id',
            'question_and_answer',
            'question_user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-question_and_answer-answer_user_id-user-id',
            'question_and_answer',
            'answer_user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-question_and_answer-answer_user_id-user-id', 'question_and_answer');
        $this->dropForeignKey('fk-question_and_answer-question_user_id-user-id', 'question_and_answer');

        $this->dropTable('question_and_answer');
    }
}
