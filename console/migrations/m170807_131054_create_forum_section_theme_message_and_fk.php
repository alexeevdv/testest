<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m170807_131054_create_forum_section_theme_message_and_fk
 */
class m170807_131054_create_forum_section_theme_message_and_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('forum_section', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer()->null(),
            'title' => $this->string(),
            'description' => $this->text()->null(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null()
        ]);

        $this->addForeignKey(
            'fk-forum_section-parent_id-forum_section-id',
            'forum_section',
            'parent_id',
            'forum_section',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('forum_theme', [
            'id' => $this->primaryKey(),
            'section_id' => $this->integer(),
            'user_id' => $this->integer(),
            'title' => $this->string(),
            'is_closed' => $this->boolean()->defaultValue(true),
            'is_pinned' => $this->boolean()->defaultValue(false),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null()
        ]);

        $this->addForeignKey(
            'fk-forum_theme-section_id-forum_section-id',
            'forum_theme',
            'section_id',
            'forum_section',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-forum_theme-user_id-user-id',
            'forum_theme',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('forum_message', [
            'id' => $this->primaryKey(),
            'theme_id' => $this->integer(),
            'user_id' => $this->integer(),
            'text' => $this->text(),
            'is_moderated' => $this->boolean()->defaultValue(false),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null()
        ]);

        $this->addForeignKey(
            'fk-forum_message-theme_id-forum_theme-id',
            'forum_message',
            'theme_id',
            'forum_theme',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-forum_message-user_id-user-id',
            'forum_message',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-forum_message-user_id-user-id', 'forum_message');
        $this->dropForeignKey('fk-forum_message-theme_id-forum_theme-id', 'forum_message');

        $this->dropTable('forum_message');

        $this->dropForeignKey('fk-forum_theme-user_id-user-id', 'forum_theme');
        $this->dropForeignKey('fk-forum_theme-section_id-forum_section-id', 'forum_theme');

        $this->dropTable('forum_theme');

        $this->dropForeignKey('fk-forum_section-parent_id-forum_section-id', 'forum_section');

        $this->dropTable('forum_section');
    }
}
