<?php

use yii\db\Migration;

/**
 * Class m170803_142607_create_type_and_organization_to_type_tbls
 */
class m170803_142607_create_type_and_organization_to_type_tbls extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('organization_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);

        $this->createTable('organization_to_type', [
            'organization_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull()
        ]);

        $this->addPrimaryKey(
            'pk-organization_id-type_id',
            'organization_to_type',
            ['organization_id', 'type_id']
        );

        $this->addForeignKey(
            'fk-organization_to_type-organization_id-organization-id',
            'organization_to_type',
            'organization_id',
            'organization',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-organization_to_type-type_id-type-id',
            'organization_to_type',
            'type_id',
            'organization_type',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-organization_to_type-type_id-type-id', 'organization_to_type');
        $this->dropForeignKey('fk-organization_to_type-organization_id-organization-id', 'organization_to_type');

        $this->dropPrimaryKey('pk-organization_id-type_id', 'organization_to_type');

        $this->dropTable('organization_to_type');
        $this->dropTable('organization_type');
    }
}
