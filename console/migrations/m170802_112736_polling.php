<?php

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m170802_112736_polling
 */
class m170802_112736_polling extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('polling', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'is_active' => $this->boolean(),
            'created_at' => $this->timestamp()->notNull()->defaultValue(new Expression('NOW()')),
            'updated_at' => $this->timestamp()->null(),
        ]);

        $this->createTable('polling_answer', [
            'id' => $this->primaryKey(),
            'polling_id' => $this->integer(),
            'text' => $this->string(),
            'has_user_variant' => $this->boolean()
        ]);

        $this->createTable('polling_answer_to_user', [
            'id' => $this->primaryKey(),
            'answer_id' => $this->integer(),
            'user_id' => $this->integer(),
            'user_answer' => $this->string()
        ]);

        $this->addForeignKey(
            'fk-polling_answer-1',
            'polling_answer',
            'polling_id',
            'polling',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk-polling_answer_to_user-1',
            'polling_answer_to_user',
            'answer_id',
            'polling_answer',
            'id',
            'cascade',
            'cascade'
        );

        $this->addForeignKey(
            'fk-polling_answer_to_user-2',
            'polling_answer_to_user',
            'user_id',
            'user',
            'id',
            'cascade',
            'cascade'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-polling_answer-1',
            'polling_answer'
        );

        $this->dropForeignKey(
            'fk-polling_answer_to_user-1',
            'polling_answer_to_user'
        );

        $this->dropForeignKey(
            'fk-polling_answer_to_user-2',
            'polling_answer_to_user'
        );

        $this->dropTable('polling');
        $this->dropTable('polling_answer');
        $this->dropTable('polling_answer_to_user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170802_112736_polling cannot be reverted.\n";

        return false;
    }
    */
}
