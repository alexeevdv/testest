<?php

use yii\db\Migration;

/**
 * Class m170804_115746_create_test_question_answer_tbls_and_fk
 */
class m170804_115746_create_test_question_answer_tbls_and_fk extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('test', [
            'id' => $this->primaryKey(),
            'title' => $this->text(),
            'is_enabled' => $this->boolean()->defaultValue(false)
        ]);

        $this->createTable('test_question', [
            'id' => $this->primaryKey(),
            'test_id' => $this->integer(),
            'text' => $this->text(),
            'is_checkbox' => $this->boolean()->comment('0 - radiobutton, 1 - checkbox'),
            'comment' => $this-> text()
        ]);

        $this->addForeignKey(
            'fk-test_question-test_id-test-id',
            'test_question',
            'test_id',
            'test',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable('test_answer', [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer(),
            'text' => $this->text(),
            'is_right' => $this->boolean()->defaultValue(true)
        ]);

        $this->addForeignKey(
            'fk-test_answer-question_id-test_question-id',
            'test_answer',
            'question_id',
            'test_question',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-test_answer-question_id-test_question-id', 'test_answer');

        $this->dropTable('test_answer');

        $this->dropForeignKey('fk-test_question-test_id-test-id', 'test_question');

        $this->dropTable('test_question');
        $this->dropTable('test');
    }
}
